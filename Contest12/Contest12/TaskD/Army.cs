using System;
using System.Collections;

public class Army: IEnumerable
{
    int[] soldiers;
    int n;

    public Army(int[] soldiers, int n)
    {
        this.soldiers = soldiers;
        this.n = n;
        if (n <= 0 || n > soldiers.Length) throw new ArgumentException("N is not valid");
    }

    public IEnumerator GetEnumerator()
    {
        for (int k = 1; k <= soldiers.Length; k++)
        {
            int numb = k * n <= soldiers.Length ? k * n : (k * n) % (soldiers.Length);
            while (numb == 0 || soldiers[numb - 1] == 0)
            {
                numb++;
                if (numb > soldiers.Length) yield break;
            }
            yield return soldiers[numb - 1];
            soldiers[numb - 1] = 0;
        }
    }
}