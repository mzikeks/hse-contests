using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class StudentReader: IDisposable, IEnumerable<Student>
{
    List<Student> students = new List<Student>();

    public StudentReader(string path)
    {
        using StreamReader streamReader = new StreamReader(path);
        string line;
        while ((line = streamReader.ReadLine()) != null)
        {
            (string surname, List<int> marks) = Student.PreprocessStudentData(line);
            students.Add(new Student(surname, marks));
        }
    }

    public void Dispose()
    {
        students = null;
    }

    public IEnumerator<Student> GetEnumerator()
    {
        return students.GetEnumerator();
    }

    public IEnumerable<Student> GetStudentsWithGreaterGpa(double gpa)
    {
        return from s in students
               where s.Gpa > gpa
               select s;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        throw new NotImplementedException();
    }
}