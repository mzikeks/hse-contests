using System;
using System.Collections.Generic;
using System.Linq;

public partial class Program
{
    private static List<Cat> ChooseCats(int minTailLength, int maxTailLength, int maxAge, List<Cat> cats)
    {
        return (from cat in cats
               where cat.TailLength <= maxTailLength && cat.TailLength >= minTailLength && cat.Age <= maxAge && cat.IsBlack
               select cat).ToList();
    }
}