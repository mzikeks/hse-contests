using System;
using System.Collections;
using System.Collections.Generic;

public class Game : IEnumerable
{
    private readonly LinkedList<int> first;
    private readonly LinkedList<int> second;
    private bool isFirst = true;

    public Game(LinkedList<int> first, LinkedList<int> second)
    {
        this.first = first;
        this.second = second;
    }

    public IEnumerator GetEnumerator()
    {
        while (true)
        {
            if (first.Count == 0)
            {
                yield return "First win!";
                yield break;
            }
            else if (second.Count == 0)
            {
                yield return "Second win!";
                yield break;
            }
            else if (isFirst)
            {
                var secondPlayerNumber = second.First.Value;
                var firstPlayerNumer = first.First.Value;
                int n = 0;
                while (secondPlayerNumber > firstPlayerNumer && n < first.Count)
                {
                    first.RemoveFirst();
                    first.AddLast(firstPlayerNumer);
                    firstPlayerNumer = first.First.Value;
                    n++;
                }
                if (n >= first.Count)
                {
                    yield return "Second win!";
                    yield break;
                }
                first.RemoveFirst();
                yield return $"First: {firstPlayerNumer}";
                isFirst = !isFirst;
            }
            else
            {
                var secondPlayerNumber = second.First.Value;
                var firstPlayerNumer = first.First.Value;
                int n = 0;
                while (firstPlayerNumer > secondPlayerNumber && n < second.Count)
                {
                    second.RemoveFirst();
                    second.AddLast(secondPlayerNumber);
                    secondPlayerNumber = second.First.Value;
                    n++;
                }
                if (n >= second.Count)
                {
                    yield return "First win!";
                    yield break;
                }
                second.RemoveFirst();
                yield return $"Second: {secondPlayerNumber}";
                isFirst = !isFirst;
            }
        }
    }
}