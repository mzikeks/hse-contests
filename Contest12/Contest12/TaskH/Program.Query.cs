using System;
using System.Collections.Generic;
using System.Linq;

public partial class Program
{
    // Сортируем по возрастанию лексикографическому города и создаём на основе их группы.
    // Далее сортируем такие группы по количеству в них пользователей. Сортировка убавающая
    // Берём пять первых групп, не включая первую.
    // В каждой такой группе необходимо сгруппировать пользователей по имени, и уже каждую такую группу преобразовать в число, равное среднему возрасту этой группы.

    private static double GetAverage(IEnumerable<User> users)
    {
        var cityGroups = users.OrderBy(u => u.City).GroupBy(u => u.City).OrderByDescending(g => g.ToArray().Length).Skip(1).Take(5);
        
        var cityNamesGroups =
            from g in cityGroups
            select g.GroupBy(u => u.Name).OrderBy(g => g.Key);
        
        var cityNamesAgesSums =
            from c in cityNamesGroups
            select c.Select(n => n.Sum(u => u.Age));

        IEnumerable<int> maxCitySums = 
            from c in cityNamesAgesSums
            select c.Max();

        return maxCitySums.Reverse().Distinct().Average();

    }
}