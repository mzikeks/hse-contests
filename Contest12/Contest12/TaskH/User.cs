using System;
using System.Linq;

public class User
{
    private ushort age;
    private string city;
    private long id;
    private string name;

    private User(long id, string name, ushort age, string city)
    {
        Id = id;
        Name = name;
        Age = age;
        City = city;
    }

    public long Id
    {
        get => id;
        private set
        {
            if (value <= 0) throw new ArgumentException();
            id = value;
        }
    }

    public string Name
    {
        get => name;
        private set
        {
            if (value.Where(c => char.IsDigit(c)).Count() > 0) throw new ArgumentException();
            name = value;
        }
    }

    public ushort Age
    {
        get => age;
        private set
        {
            if (value > 128) throw new ArgumentException();
            age = value;
        }
    }

    public string City
    {
        get => city;
        private set
        {
            city = value;
        }
    }

    public static User Parse(string str)
    {
        var splited = str.Split(";");
        try
        {
            long id = long.Parse(splited[0]);
            string name = splited[1];
            ushort age = ushort.Parse(splited[2]);
            string city = splited[3];
            return new User(id, name, age, city);
        }
        catch
        {
            throw new ArgumentException("Incorrect input");
        }
    }
}