using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class OnlyEven: IEnumerable<int>
{
    private IEnumerable<int> numbers;
    
    public OnlyEven(List<int> numbers)
    {
        this.numbers = from n in numbers
                       where n % 2 == 0
                       select n;
    }

    public IEnumerator<int> GetEnumerator()
    {
        return numbers.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        throw new NotImplementedException();
    }
}