using System;
using System.Linq;

public class User
{
    private long id;
    private string name;
    private ushort age;

    public long Id
    {
        get => id;
        private set
        {
            if (value <= 0) throw new ArgumentException();
            id = value;
        }
    }
    
    public string Name
    {
        get => name;
        private set
        {
            if (value.Where(c => char.IsDigit(c)).Count() > 0) throw new ArgumentException();
            name = value;
        }
    }
    
    public ushort Age
    {
        get => age;
        private set
        {
            if (value > 128) throw new ArgumentException(); 
            age = value;
        }
    }

    private User(long id, string name, ushort age)
    {
        Id = id;
        Name = name;
        Age = age;
    }

    public static User Parse(string str)
    {
        var splited = str.Split(";");
        try
        {
            long id = long.Parse(splited[0]);
            string name = splited[1];
            ushort age = ushort.Parse(splited[2]);
            return new User(id, name, age);
        }
        catch
        {
            throw new ArgumentException("Incorrect input");
        }
    }
}