﻿using System;
using System.Linq;

public class Program
{
    public static void Main(string[] args)
    {
        var tshorts = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var shorts = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int sU = 0;
        int minT = 0;
        int minS = int.MaxValue;

        foreach (var t in tshorts)
        {
            for (int i = sU; i < shorts.Length; i++)
            {
                if (i + 1 < shorts.Length && Math.Abs(t - shorts[i]) < Math.Abs(t - shorts[i + 1]))
                { 
                    if (Math.Abs(shorts[i] - t) < Math.Abs(minS - minT))
                    {
                        minS = shorts[i];
                        minT = t;
                    }
                    sU = i;
                    break;
                }
                else if (i + 1 >= shorts.Length)
                {
                    if (Math.Abs(shorts[i] - t) < Math.Abs(minS - minT))
                    {
                        minS = shorts[i];
                        minT = t;
                    }
                    sU = i;
                    break;
                }
            }
            if (minT - minS == 0) break;
        }
        Console.WriteLine($"{minT} {minS}");
    }
}