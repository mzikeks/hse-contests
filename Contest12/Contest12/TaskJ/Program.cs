﻿
using System;
using System.Collections.Generic;
using System.Linq;

public class Program
{
    public static void Main(string[] args)
    {
        string input = Console.ReadLine();
        string symbolsToReplace = "<>/abcdefghjiklmnopqrstuvwxyz";

        foreach (char symbol in symbolsToReplace)
        {
            for (int i = 0; i < input.Length; i++)
            {
                string newString = input.Remove(i, 1).Insert(i, symbol.ToString());
                if (CheckCorrectXML(newString))
                {
                    Console.WriteLine(newString);
                    return;
                }
            }
        }
    }

    public static bool CheckOpenCloseTagsCount(List<Tag> tags)
    {
        if (tags == null) return false;
        var q = (from tag in tags
                 select tag.isOpen).Count(t => t == true);

        return tags.Count - q == q;
    }

    public static bool CheckCorrectXML(string xml)
    {
        List<Tag> tags;

        tags = GetTags(xml);
        if (tags == null) return false;

        var OpenedTags = new LinkedList<Tag>();

        foreach (Tag tag in tags)
        {
            if (tag.isOpen)
            {
                OpenedTags.AddLast(tag);
            }
            else
            {
                if (OpenedTags.Count == 0) return false;

                var openTag = OpenedTags.Last;
                OpenedTags.RemoveLast();
                if (openTag.Value.name != tag.name) return false;
            }
        }
        if (OpenedTags.Count > 0) return false;


        return true;
    }

    public static List<Tag> GetTags(string xml)
    {
        bool isOpen = false;
        int openIndex = 0;
        List<Tag> tags = new List<Tag>();

        for (int i = 0; i < xml.Length; i++)
        {
            if (xml[i] == '<')
            {
                if (isOpen) return null;
                openIndex = i;
                isOpen = true;
            }
            else if (xml[i] == '>')
            {
                if (!isOpen) return null;
                bool tagTypeOpen = xml[openIndex + 1] != '/';
                tags.Add(new Tag
                {
                    isOpen = tagTypeOpen,
                    name = (tagTypeOpen ? xml.Substring(openIndex + 1, i - openIndex - 1) : xml.Substring(openIndex + 2, i - openIndex - 2))
                });
                isOpen = false;
            }
        }
        return tags;

    }
}

public class Tag
{
    public bool isOpen;
    public string name;
}