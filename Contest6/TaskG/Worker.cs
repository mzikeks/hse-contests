﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

public class Worker
{
    public Apple[] Apples {get; set;}
    
    public Worker(Apple[] apples)
    {
        Array.Sort(apples, (x, y) => (x.Weight > y.Weight) ? 1 : -1);
        this.Apples = apples;
    }

    public Apple[] GetSortedApples()
    {
        return Apples;
    }
}