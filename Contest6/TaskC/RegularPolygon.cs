using System;

public class RegularPolygon : Polygon
{
    private double area;

    private double perimeter;

    private double side;

    private double numberOfSides;

    public override double Area => area;

    public override double Perimeter => perimeter;
    
    public RegularPolygon(double side, int numberOfSides)
        :base()
    {
        if (side <= 0) throw new ArgumentException("Side length value should be greater than zero.");
        if (numberOfSides < 3) throw new ArgumentException("Number of sides value should be greater than 3.");
        this.area = side * side * numberOfSides / 4 / Math.Tan(Math.PI/ numberOfSides);
        this.perimeter = side * numberOfSides;

        this.numberOfSides = numberOfSides;
        this.side = side;
    }
    public override string ToString() => $"side: {side}; numberOfSides: {numberOfSides}; area: {Area:f3}; perimeter: {Perimeter:f3}";

}