﻿using System;

public static class TemperatureCalculator
{
    static double kelvinMin = -273.15;
    public static double FromCelsiusToFahrenheit(double celsiusTemperature)
    {
        return (celsiusTemperature < -273.15) ? throw new ArgumentException("Incorrect input") : celsiusTemperature * 9 / 5 + 32;
    }

    public static double FromCelsiusToKelvin(double celsiusTemperature)
    {
        return (celsiusTemperature < -273.15) ? throw new ArgumentException("Incorrect input") : celsiusTemperature - kelvinMin;
    }

    public static double FromFahrenheitToCelsius(double fahrenheitTemperature)
    {
        return (fahrenheitTemperature < -459.67) ? throw new ArgumentException("Incorrect input") : (fahrenheitTemperature - 32) * 5 / 9;
    }

    public static double FromFahrenheitToKelvin(double fahrenheitTemperature)
    {
        return (fahrenheitTemperature < -459.67) ? throw new ArgumentException("Incorrect input") : FromCelsiusToKelvin(FromFahrenheitToCelsius(fahrenheitTemperature));
    }

    public static double FromKelvinToCelsius(double kelvinTemperature)
    {
        return (kelvinTemperature < 0) ? throw new ArgumentException("Incorrect input") : kelvinTemperature + kelvinMin;
    }

    public static double FromKelvinToFahrenheit(double kelvinTemperature)
    {
        return (kelvinTemperature < 0) ? throw new ArgumentException("Incorrect input") : FromCelsiusToFahrenheit(FromKelvinToCelsius(kelvinTemperature));
    }

}
