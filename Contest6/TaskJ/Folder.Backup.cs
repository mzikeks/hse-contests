﻿using System;
using System.Collections.Generic;
using System.Text;

partial class Folder
{
    internal class Backup
    {
        List<File> files = new List<File>();

        public Backup(Folder folder)
        {
            foreach (File file in folder.files)
            {
                files.Add(new File(file));
            }
        }


        public static void Restore(Folder folder, Backup backup)
        {
            var restoredCopy = new List<File>();
            foreach (File file in backup.files)
            {
                restoredCopy.Add(new File(file));
            }
            folder.files = restoredCopy;
        }

    }

    public void AddFile(string name, int size)
    {
        files.Add(new File(name, size));
    }

    public void RemoveFile(File file)
    {
        files.Remove(this[file.Name]);
    }

    public File this[int i]
    {
        get {
            try
            {
                return files[i];
            }
            catch
            {
                throw new IndexOutOfRangeException("Not enough files or index less zero");
            }
        }
    }

    public File this[string filename]
    {
        get {
            foreach (File file in files)
            {
                if (file.Name == filename) return file;
            }
            throw new ArgumentException("File not found");
        }
    }

    public override string ToString()
    {
        var sb = new StringBuilder("Files in folder:" + Environment.NewLine);
        foreach (File file in files)
        {
            sb.Append(file.ToString() + Environment.NewLine);
        }
        return sb.Remove(sb.Length - 1, 1).ToString();
    }
}

