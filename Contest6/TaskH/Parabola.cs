﻿using System;

public class Parabola : Function
{
    public override double GetValueInX(double x)
    {
        return x * x + x + 7;
    }

    public override bool IsBordersCorrect(double a, double b)
    {
        return true;
    }
}
