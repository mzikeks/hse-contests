﻿using System;

public abstract class Function
{
    public static Function GetFunction(string functionName)
    {
        if (functionName == "Sin") return new Sin();
        else if (functionName == "Exp") return new Exponent();
        else if (functionName == "Parabola") return new Parabola();
        else throw new ArgumentException("Incorrect input");
    }

    public abstract double GetValueInX(double x);
    
    public abstract bool IsBordersCorrect(double a, double b);

    public static double SolveIntegral(Function func, double left, double right, double step)
    {
        if (left > right) throw new ArgumentException("Left border greater than right");
        if (!func.IsBordersCorrect(left, right)) throw new ArgumentException("Function is not defined in point");

        double s = 0;
        
        double point;
        for (point = left; point <= right - step; point += step)
        {
            s += 0.5 * (func.GetValueInX(point) + func.GetValueInX(point + step)) * step;
        }
        s += 0.5 * (func.GetValueInX(point) + func.GetValueInX(right)) * (right - point);
       
        return s;
    }
}
