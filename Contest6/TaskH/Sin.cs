﻿using System;

public class Sin : Function
{
    public override double GetValueInX(double x)
    {
        return 1 / Math.Sin(x);       
    }
    public override bool IsBordersCorrect(double a, double b)
    {
        if (Math.Sin(a) != 0 && Math.Sin(b) != 0) return true;
        return false;
    }
}
