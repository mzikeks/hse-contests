﻿using System;

public class Exponent : Function
{
    public override double GetValueInX(double x)
    {
        return Math.Pow(Math.E, 1 / x);
    }
    public override bool IsBordersCorrect(double a, double b)
    {
        if (a != 0 && b != 0) return true;
        return false;
    }
}
