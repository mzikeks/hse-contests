using System;
using System.IO;

public class Wizard : LegendaryHuman
{
    string rank;
    int rankN;

    public Wizard(string name, int healthPoints, int power, string rank) 
        : base(name, healthPoints, power)
    {
        this.rank = rank;
        switch (rank)
        {
            case "Neophyte":
                rankN = 1;
                break;
            case "Adept":
                rankN = 2;
                break;
            case "Charmer":
                rankN = 5;
                break;
            case "Sorcerer":
                rankN = 8;
                break;
            case "Master":
                rankN = 11;
                break;
            case "Archmage":
                rankN = 14;
                break;
            default:
                throw new ArgumentException("Invalid wizard rank.");
        }
    }

    public override void Attack(LegendaryHuman enemy)
    {
        if (HealthPoints <= 0 || enemy.HealthPoints <= 0) return;

        int power = Power * rankN + HealthPoints / 10;

        Console.WriteLine($"{this} attacked {enemy}.");

        enemy.HealthPoints -= power;

        if (enemy.HealthPoints <= 0)
        {
            Console.WriteLine($"{enemy} is dead.");
        }
    }

    public override string ToString()
    {
        return $"{rank} Wizard {Name} with HP {HealthPoints}";
    }
}