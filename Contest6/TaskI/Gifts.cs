﻿using System;

public static class GiftCreator
{
    public static Gift CreateGift(string giftName)
    {
        if (giftName == "Phone") return new Phone();
        else if (giftName == "PlayStation") return new PlayStation();
        throw new Exception();
    }
}

public class Phone : Gift
{
    public static int nextId = 0;
    public Phone()
        :base(nextId)
    {
        nextId++;
    }
}

public class PlayStation : Gift
{
    public static int nextId = 0;
    public PlayStation()
        :base(nextId)
    {
        nextId++;
    }
}

