﻿using System;
using System.Collections.Generic;

public class Grassland
{
    List<Sheep> sheeps;

    public Grassland(List<Sheep> sheeps)
    {
        this.sheeps = sheeps;
    }

    public List<Sheep> GetEvenSheeps()
    {
        var result = new List<Sheep>();
        foreach (Sheep sheep in sheeps)
        {
            if (sheep.Id % 2 == 0) result.Add(sheep); 
        }
        return result;
    }

    public List<Sheep> GetOddSheeps()
    {
        var result = new List<Sheep>();
        foreach (Sheep sheep in sheeps)
        {
            if (sheep.Id % 2 != 0) result.Add(sheep);
        }
        return result;
    }

    public List<Sheep> GetSheepsByContainsName(string name)
    {
        var result = new List<Sheep>();
        foreach (Sheep sheep in sheeps)
        {
            if (sheep.Name.Contains(name)) result.Add(sheep);
        }
        return result;
    }
}
