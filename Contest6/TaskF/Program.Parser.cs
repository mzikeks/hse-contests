﻿using System;


public partial class Program
{
    static Sheep ParseSheep(string line)
    {
        string[] input = line.Split(new string[] { "Sheep ", " with Id ", " makes " }, StringSplitOptions.RemoveEmptyEntries);
        if (input.Length != 3)
        {
            throw new ArgumentException("Incorrect input");
        }
        string name = input[0];
        string sound = input[2];
        int id;
        try
        {
            id = int.Parse(input[1]);
            if (id <= 0 || id >= 1000) throw new ArgumentException();
        }
        catch
        {
            throw new ArgumentException("Incorrect input");
        }
        return new Sheep(id, name, sound);
    }
}
