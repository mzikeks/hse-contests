﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class IntWrapper
{
    int number;
    public IntWrapper(int number)
    {
        this.number = number; 
    }

    public int FindNumberLength()
    {
        int lenght = 0;
        do { number /= 10; lenght++; }
        while (number > 0);
        return (number < 0) ? throw new ArgumentException("Number should be non-negative."): lenght;
    }
}
