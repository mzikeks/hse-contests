using System;

public class Download<T> : IDownload where T : Content
{
    private readonly T download;

    public Download(T download)
    {
        this.download = download;
    }

    public bool DownloadContent()
    {
        if (download.Size <= Program.FreeSpace)
        {
            Console.WriteLine($"{download.Size}/{download.Size} MB");
            Program.FreeSpace -= download.Size;
            return true;
        }
        Console.WriteLine($"{Program.FreeSpace}/{download.Size} MB");
        Console.WriteLine("Not enough free space!\n");
        return false;

    }
}