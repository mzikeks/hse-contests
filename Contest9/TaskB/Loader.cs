using System;
using System.Collections.Generic;

public class Loader
{
    private static Dictionary<string, int> memory = new Dictionary<string, int>();

    public static void AddValueToStore(string key)
    {
        if (!memory.ContainsKey(key))
        {
            memory.Add(key, 0);
        }
        memory[key] += 1;
    }

    public static void Download(IList<IDownload> downloadsQueue)
    {
        foreach (var download in downloadsQueue)
        {
            if (download.DownloadContent())
            {
                AddValueToStore(GetKey(download.GetType()));
            }
            else
            {
                break;
            }
        }
        Console.WriteLine("Downloaded content:");
        foreach (var key in memory.Keys)
        {
            Console.WriteLine($"{key}: {memory[key]}");
        }
    }

    private static string GetKey(Type type)
    {
        return type.ToString().Split(new char[2] { '[', ']' })[1];
    }
}