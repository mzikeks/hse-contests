using System;
using System.Collections.Generic;

public class TeamLead : Programmer
{
    private readonly List<Programmer> programmers;

    public TeamLead(int id, List<Programmer> programmers) : base(id)
    {
        this.programmers = programmers;
    }

    public List<Programmer> Programmers
    {
        get { return programmers; }
    }

    public void HuntProgrammers(List<TeamLead> teamLeads)
    {
        foreach (var teamLead in teamLeads)
        {
            if (teamLead == this) continue;
            var huntedProgrammers = new List<Programmer>();
            foreach (var programmer in teamLead.Programmers)
            {
                if (programmer.LinesOfCode % (this.Id + 1) == 0)
                {
                    this.programmers.Add(programmer);
                    huntedProgrammers.Add(programmer);
                }
            }
            foreach (var programmer in huntedProgrammers) teamLead.Programmers.Remove(programmer);
        }
    }

    public int GetWrittenLinesOfCode()
    {
        int sum = 0;
        foreach (var programmer in Programmers)
        {
            sum += programmer.LinesOfCode;
        }
        return sum + this.LinesOfCode;
    }

    public override string ToString()
    {
        return String.Format("Team lead #{0}\nAmount of programmers in team: {1}\nWritten lines of code: {2}", 
            Id, programmers.Count, GetWrittenLinesOfCode());
    }
}