using System;
using System.Collections.Generic;

public class Company
{
    private readonly List<TeamLead> teamLeads;

    public Company(int teamLeadsAmount, int[] programmersAmount)
    {
        this.teamLeads = new List<TeamLead>();
        for (int i = 0; i < teamLeadsAmount; i++)
        {
            var programmers = new List<Programmer>();
            for (int j = 0; j < programmersAmount[i]; j++)
            {
                programmers.Add(new Programmer(int.Parse((i+1).ToString()+(j+1).ToString())));
            }
            teamLeads.Add(new TeamLead(i + 1, programmers));
        }
    }

    public List<TeamLead> TeamLeads
    {
        get { return teamLeads; }
    }

    public void PrintTeams()
    {
        foreach (TeamLead teamLead in TeamLeads)
        {
            Console.WriteLine(teamLead);
        }
        Console.WriteLine();
    }
}