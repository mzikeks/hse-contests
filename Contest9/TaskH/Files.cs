using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Files
{
    Dictionary<string, Permissions> database = new Dictionary<string, Permissions>();

    public void CreateFile(string filename)
    {
        database.Add(filename, Permissions.Default);
    }

    public void AddPermission(string filename, string permissionName)
    {
        database[filename] = database[filename] | PermissionBuilder.FromName(permissionName);
    }

    public void RemovePermission(string filename, string permissionName)
    {
        if (database[filename].HasFlag(PermissionBuilder.FromName(permissionName))) 
        {
            database[filename] = database[filename] ^ PermissionBuilder.FromName(permissionName);
        }
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        foreach (var key in database.Keys)
        {
            sb.Append($"{key}: {database[key].ToString().Replace(",", "")}\n");
        }
        return sb.ToString();
    }
}