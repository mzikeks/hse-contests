﻿using System;

public class Program
{
    public static void Main(string[] args)
    {
        var temperatures = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);

        for (int i = 0; i < temperatures.Length; i++)
        {
            int t = temperatures[i];
            int count = 0;
            for (int j = i+1; j < temperatures.Length; j++)
            {
                if (temperatures[j] > temperatures[i])
                {
                    count = j - i;
                    break;
                }
            }
            Console.Write(count + " ");
        }

    }
}