using System;

public class Vector : IComparable
{
    int x, y;

    public Vector(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public double Length
    {
        get { return Math.Sqrt(x*x + y*y); }
    }

    internal static Vector Parse(string input)
    {
        try
        {
            var vecArgs = Array.ConvertAll(input.Split(), int.Parse);
            if (vecArgs.Length == 2) return new Vector(vecArgs[0], vecArgs[1]);
        }
        catch {}
        throw new ArgumentException("Incorrect vector");
    }

    public int CompareTo(object second)
    {
        if (((Vector)second).Length == Length) return 0;
        else if (((Vector)second).Length < Length) return 1;
        return -1;
    }
}