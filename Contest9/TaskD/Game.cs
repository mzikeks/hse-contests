using System;
using System.Collections.Generic;

public class Game
{
    public static IList<IHelper> helpers = new List<IHelper>();
    public static int numberOfPlayedRounds = 0;
    private readonly int numberOfRounds;

    public Game(int numberOfHeroes, int numberOfPlumbers, int numberOfMarios, int numberOfRounds)
    {
        this.numberOfRounds = numberOfRounds;
        for (int i = 0; i < numberOfHeroes; i++) helpers.Add(new Hero());
        for (int i = 0; i < numberOfPlumbers; i++) helpers.Add(new Plumber());
        for (int i = 0; i < numberOfMarios; i++) helpers.Add(new Mario());
    }

    public void Play()
    {
        for (int i = 0; i < numberOfRounds; i++)
        {
            int[] roundArgs = null;
            try
            {
                roundArgs = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            }

            catch { }

            if (roundArgs == null || roundArgs.Length != 2)
            {
                Console.WriteLine("Incorrect data!");
                i--;
                continue;
            }

            Round round = new Round(roundArgs[0], roundArgs[1]);
            round.Play(helpers);
        }
    }
}