public class Mario : IHelper, IHero, IPlumber
{
    public void FixPipe(ref int numberOfCrashes)
    {
        numberOfCrashes--;
    }

    public void KillMonster(ref int numberOfMonsters)
    {
        numberOfMonsters--;
    }
}