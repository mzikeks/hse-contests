using System;
using System.Collections.Generic;

public class Round
{
    private int amountOfMonsters;
    private int amountOfCrashes;

    public Round(int amountOfMonsters, int amountOfCrashes)
    {
        this.amountOfCrashes = amountOfCrashes;
        this.amountOfMonsters = amountOfMonsters;
    }

    public void Play(IList<IHelper> helpers)
    {
        foreach (IHelper helper in helpers)
        {
            if (helper is IHero) ((IHero)helper).KillMonster(ref amountOfMonsters);
            if (helper is IPlumber) ((IPlumber)helper).FixPipe(ref amountOfCrashes);
        }
        if (amountOfMonsters <= 0 && amountOfCrashes <= 0)
        {
            Console.WriteLine("Helpers won this round!");
            return;
        }
        Console.WriteLine("Helpers lost this round!");
        if (amountOfCrashes > 0 && amountOfMonsters > 0) helpers.Add(new Mario());
        else if (amountOfCrashes > 0) helpers.Add(new Plumber());
        else helpers.Add(new Hero());
    }
}