using System;
using System.Collections.Generic;

public static class Methods
{
    public static int FindBestBiathlonistValue(List<Sportsman> sportsmen)
    {
        int max = 0;
        foreach (Sportsman sportsman in sportsmen)
        {
            int Shoot = 0;
            int Run = 0;
            if (sportsman is IShooter) Shoot = ((IShooter)sportsman).Shoot();
            if (sportsman is ISkiRunner) Run = ((ISkiRunner)sportsman).Run();
            max = Math.Max(max, GetBiatlonistValue(Shoot, Run));
        }
        return max;
    }

    public static int FindBestShooterValue(List<Sportsman> sportsmen)
    {
        int max = 0;
        foreach (Sportsman sportsman in sportsmen)
        {
            if (sportsman is IShooter) max = Math.Max(max, ((IShooter)sportsman).Shoot());
        }
        return max;
    }

    public static int FindBestRunnerValue(List<Sportsman> sportsmen)
    {
        int max = 0;
        foreach (Sportsman sportsman in sportsmen)
        {
            if (sportsman is ISkiRunner) max = Math.Max(max, ((ISkiRunner)sportsman).Run());
        }
        return max;
    }

    public static int GetBiatlonistValue(int Shoot, int Run)
    {
        return (int)(0.4 * Math.Max(Shoot, Run) + 0.6 * Math.Min(Shoot, Run));
    }
}