using System;
using System.Diagnostics.CodeAnalysis;

internal struct Student : IComparable<Student>
{
    private int id;
    private int height;
    private int math;
    private int english;
    private int PE;
    private bool isMath;

    public Student(int id, int height, int math, int english, int PE)
    {
        this.id = id;
        this.height = height;
        this.math = math;
        this.english = english;
        this.PE = PE;
        this.isMath = math > PE;
    }

    internal static Student Parse(string v)
    {
        var studentArgs = Array.ConvertAll(v.Split(), int.Parse);
        return new Student(studentArgs[0], studentArgs[1], studentArgs[2], studentArgs[3], studentArgs[4]);
    }

    public int CompareTo([AllowNull] Student other)
    {
        if (math > other.math) return 1;
        else if (math < other.math) return -1;
        else
        {
            if (english > other.english) return 1;
            else return -1;
        }
    }


    int IComparable<Student>.CompareTo([AllowNull] Student other)
    {
        if (PE > other.PE) return 1;
        else if (PE < other.PE) return -1;
        else
        {
            if (height > other.height) return 1;
            else return -1;
        }
    }

    public override string ToString()
    {
         return id.ToString();
    }
}