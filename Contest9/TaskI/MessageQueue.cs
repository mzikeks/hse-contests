using System;
using System.Collections.Generic;

public class MessageQueue
{
    private List<Message> queue = new List<Message>();
    private int size;

    public MessageQueue(int size)
    {
        this.size = size;
    }

    public void Push(Message message)
    {
        queue.Add(message);
        if (queue.Count > size) throw new KafkaException("Queue is out of storage");
    }

    public int Size
    {
        get { return size; }
    }

    public List<Message> this[int from, int to]
    {
        get
        {
            try
            {
                return queue.GetRange(from, to);
            }
            catch
            {
                throw new KafkaException("Not enough messages");
            }
        }
    }

    public Message this[int i]
    {
        get
        {
            return queue[i];
        }
    }
}