using System;
using System.Collections.Generic;

public class Kafka
{
    private MessageQueue queue;
    private HashSet<User> allowed = new HashSet<User>();
    private bool isActive = false;

    public Kafka(int queueSize)
    {
        this.queue = new MessageQueue(queueSize);
    }

    public void Subscribe(User user)
    {
        CheckActive();
        if (allowed.Contains(user)) throw new KafkaException("User is already subscribed");
        allowed.Add(user);
    }

    public void Unsubscribe(User user)
    {
        CheckActive(); 
        if (!allowed.Contains(user)) throw new KafkaException("User is already unsubscribed");
        allowed.Remove(user);
    }

    public void Push(Message message)
    {
        CheckActive();
        queue.Push(message);
    }

    public List<Message> PopMessages(User user, int count)
    {
        CheckActive();
        var messages = queue[user.Index, count];
        if (!allowed.Contains(user)) throw new KafkaException("User is not subscribed");
        user.IncreaseIndex(count);
        return messages;
    }

    public void Activate()
    {
        this.isActive = true;
    }

    public void Deactivate()
    {
        CheckActive();
        this.isActive = false;
    }

    private void CheckActive()
    {
        if (isActive == false) throw new KafkaException("Kafka is not active");
    }
}