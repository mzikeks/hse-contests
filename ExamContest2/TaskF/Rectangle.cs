﻿using System;
using System.Collections.Generic;

public sealed class Rectangle : GeometryRef
{
    Triangle triangle;
    public Rectangle(List<Point> points) : base(points)
    {
        this.triangle = new Triangle(new List<Point> { points[0], points[1], points[2] });
        type = "Rectangle";
    }

    protected override double GetPerimeter()
    {
        if (triangle.AB > triangle.AC && triangle.AB > triangle.BC) return triangle.BC * 2 + triangle.AC * 2;
        if (triangle.BC > triangle.AC && triangle.BC > triangle.AB) return triangle.AB * 2 + triangle.AC * 2;
        else return triangle.BC * 2 + triangle.AB * 2;
    }

    public override double GetSquare()
    {
        return triangle.GetSquare() * 2;
    }
}