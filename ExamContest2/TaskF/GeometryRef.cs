﻿using System;
using System.Collections.Generic;

public class GeometryRef
{
    protected List<Point> points;
    protected string type = "GeometryRef";

    protected virtual List<Point> Points
    {
        get => points;
        set { points = value; }
    }

    protected GeometryRef(List<Point> points)
    {
        Points = points;
    }

    protected virtual double GetPerimeter()
    {
        return 0;
    }

    public virtual double GetSquare()
    {
        return 0;
    }

    public static GeometryRef Parse(string line)
    {
        string[] input = line.Split();
        string name = input[0];
        try
        {
            List<Point> points = new List<Point>();
            for (int i = 1; i < input.Length; i+=2)
            {
                points.Add(new Point(double.Parse(input[i]), double.Parse(input[i+1])));
            }
            if (name == "Triangle")
            {
                if (input.Length != 7) throw new Exception();
                return new Triangle(points);
            }
            else if (name == "Rectangle")
            {
                if (input.Length != 9) throw new Exception();
                return new Rectangle(points);
            }
            else if (name == "GeometryRef")
            { 
                if (input.Length % 2 == 0) throw new Exception();
                return new GeometryRef(points);
            }
            throw new Exception();
            
        }
        catch
        {
            throw new ArgumentException("Incorrect input");
        }
    }

    public static double GetDistance(Point A, Point B)
    {
        return Math.Sqrt((A.X - B.X) * (A.X - B.X) + (A.Y - B.Y) * (A.Y - B.Y));
    }
    public override string ToString()
    {
        return $"{type}. P = {GetPerimeter().ToString("f2")}. S = {GetSquare().ToString("f2")}.";
    }
}