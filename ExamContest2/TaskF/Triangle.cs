﻿using System;
using System.Collections.Generic;

public sealed class Triangle : GeometryRef
{
    Point A;
    Point B;
    Point C;
    public double p;
    public double AB;
    public double BC;
    public double AC;

    public Triangle(List<Point> points) : base(points)
    {
        this.A = points[0];
        this.B = points[1];
        this.C = points[2];
        this.AB = GetDistance(A, B);
        this.AC = GetDistance(A, C);
        this.BC = GetDistance(B, C);
        p = (AB + BC + AC) / 2;
        type = "Triangle";

    }

    protected override double GetPerimeter()
    {
        return p * 2;
    }

    public override double GetSquare()
    {
        //  Console.WriteLine($"{AB} {BC} {AC}");
        return Math.Sqrt(p * (p - AB) * (p - AC) * (p - BC));
    }
}