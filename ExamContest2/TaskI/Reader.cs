using System;
using System.Collections.Generic;
using System.IO;

public static class Reader
{
    public static int[] ReadFile(string fileName)
    {
        string[] input = File.ReadAllLines(fileName)[0].Split();
        int[] numbers = new int[input.Length];
        
        for (int i = 0; i < input.Length; i++)
        {
            numbers[i] = ParseWord(input[i]);
        }
        return numbers;
    }

    private static int ParseWord(string word)
    {
        bool isNumbStart = false;
        var numbers = new List<int>();
        int sign = 1;
        foreach (char symb in word)
        {
            if (symb >= '0' && symb <= '9')
            {
                numbers.Add(int.Parse(symb.ToString()));
                isNumbStart = true;
            }
            if (symb == '-' && !isNumbStart) sign = sign * (-1);
        }
        numbers.Reverse();
        int n = 0;
        for (int i = 0; i < numbers.Count; i++)
        {
            n += (int)Math.Pow(10, i) * numbers[i];
        }
        return n * sign;
    }
}