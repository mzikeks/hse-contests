﻿using System;
using System.Collections.Generic;

public class Mushroom
{
    private string Name { get; set; }
    private double Weight { get; set; }
    private double Diameter { get; set; }

    private Mushroom(string name, double weight, double diameter)
    {
        Name = name;
        Weight = weight;
        Diameter = diameter;
    }

    public static Mushroom Parse(string line)
    {
        var input = line.Split();
        if (input.Length != 3) throw new ArgumentException("Incorrect input");
        string name = input[0];
        if (double.TryParse(input[1], out double weigth) && double.TryParse(input[2], out double diameter) &&
            diameter > 0 && weigth > 0)
        {
            return new Mushroom(name, weigth, diameter);
        }
        throw new ArgumentException("Incorrect input");
    }

    public static double GetAverageDiameter(List<Mushroom> mushrooms, double m)
    {
        int n = 0;
        double sum = 0;
        foreach (Mushroom mushroom in mushrooms)
        {
            if (mushroom.Weight > m)
            {
                n++;
                sum += mushroom.Diameter;
            }
        }
        return n > 0 ? sum / n : 0;
    }
}