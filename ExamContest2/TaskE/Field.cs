using System;
using System.Text;

public class Field
{
    private int[][] matrix;
    int n;

    public Field(int[][] matrix)
    {
        this.matrix = new int[matrix.Length][];
        this.n = matrix.Length;
    }

    public void FillIn(string fillType)
    {
        if (fillType == "left to right")
        {
            for (int i = 0; i < n; i++)
            {
                matrix[i] = new int[n];
                int nextElement = i;
                for (int j = 0; j < n; j++)
                {
                    matrix[i][j] = ++nextElement;
                }
            }

        }
        else if (fillType == "right to left")
        {
            for (int i = 0; i < n; i++)
            {
                matrix[i] = new int[n];
                int nextElement = i;
                for (int j = n-1; j >= 0; j--)
                {
                    matrix[i][j] = ++nextElement;
                }
            }
        }
        else throw new ArgumentException("Incorrect input");
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        foreach (int[] s in matrix)
        {
            sb.Append(string.Join(' ', s));
            sb.Append('\n');
        }
        sb.Remove(sb.Length - 1, 1);
        return sb.ToString();
    }
}