using System;
using System.Collections.Generic;

public class Server
{
    public string name;
    public static Server connectedServer = new Server();
    public static List<string> messages = new List<string>();
    public Server()
    {

    }

    public static Server Connect(string name)
    {
        if (connectedServer.name != null)
        {
            return connectedServer;
        }
        var s = new Server
        {
            name = name
        };
        connectedServer = s;
        return s;
        
    }

    public void Send(string message)
    {
        if (this.name == null) throw new ArgumentException("No connected server");
        messages.Add($"Sending data {message} to server {this.name}");
    }

    public void Receive(string message)
    {
        if (this.name == null) throw new ArgumentException("No connected server");
        messages.Add($"Receiving data {message} from server {this.name}");
    }

    public void Output()
    {
        if (this.name == null) throw new ArgumentException("No connected server");
        foreach (string message in messages) Console.WriteLine(message);
        messages = new List<string>();
    }
}