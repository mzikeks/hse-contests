{\rtf1\ansi\ansicpg1251\deff0\nouicompat\deflang1049{\fonttbl{\f0\fnil\fcharset204 Segoe UI;}}
{\*\generator Riched20 10.0.18362}\viewkind4\uc1 
\pard\f0\fs18 using System;\par
\par
internal class ChristmasArray : BaseArray\par
\{\par
    public ChristmasArray(int[] array) : base(array)\par
    \{\par
\par
    \}\par
\par
    public override int this[int number]\par
    \{\par
        get\par
        \{\par
            long minimumDiff = long.MaxValue;\par
            int nearestNIndex = -1;\par
            for (int i = 0; i < array.Length; i++)\par
            \{\par
                if (array[i] < number && Math.Abs(array[i] - number) < minimumDiff) \par
                \{\par
                    minimumDiff = Math.Abs(array[i] - number);\par
                    nearestNIndex = i;\par
                \}\par
            \}\par
            if (nearestNIndex == -1)\par
            \{\par
                throw new ArgumentException("Number does not exist.");\par
            \}\par
            return array[nearestNIndex];\par
        \}\par
    \}\par
\par
    public override double GetMetric()\par
    \{\par
        double nSixs = 0;\par
        double nTotal = 0;\par
        foreach (int element in array)\par
        \{\par
            int e = element;\par
            while (e > 0)\par
            \{\par
                nTotal++;\par
                if (e % 10 == 6) nSixs++;\par
                e /= 10;\par
            \}\par
        \}\par
        return nSixs / nTotal;\par
    \}\par
\}\par
}
 