{\rtf1\ansi\ansicpg1251\deff0\nouicompat\deflang1049{\fonttbl{\f0\fnil\fcharset204 Segoe UI;}}
{\*\generator Riched20 10.0.18362}\viewkind4\uc1 
\pard\f0\fs18 public abstract class BaseArray\par
\{\par
    protected int[] array;\par
\par
    public BaseArray(int[] array)\par
    \{\par
        this.array = array;\par
    \}\par
    \par
    public abstract int this[int number] \{ get; \}\par
\par
    public abstract double GetMetric();\par
\}\par
}
