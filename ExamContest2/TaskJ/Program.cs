﻿using System;
using System.Collections.Generic;

class Program
{
    public static void Main(string[] args)
    {
        var s1 = Console.ReadLine().Split();
        var s2 = Console.ReadLine().Split();

        var p1 = new int[s1.Length];
        var p2 = new int[s2.Length];

        for (int i = 0; i < s1.Length; i++) p1[i] = int.Parse(s1[i]);
        for (int i = 0; i < s2.Length; i++) p2[i] = int.Parse(s2[i]);

        //возьмем все случаи из p1;
        int maxLength = 0;

        for (int i = 0; i < Math.Pow(2, p1.Length); i++)
        {
            int q1 = i;
            int nIter1 = 0;
            var perm1 = new List<int>();
            while (q1 > 0)
            {
                if (q1 % 2 != 0) perm1.Add(p1[nIter1]);
                q1 /= 2;
                nIter1++;
            }

            for (int j = 0; j < Math.Pow(2, p2.Length); j++)
            {
                int q2 = j;
                int nIter2 = 0;
                var perm2 = new List<int>();
                while (q2 > 0)
                {
                    if (q2 % 2 != 0) perm2.Add(p2[nIter2]);
                    q2 /= 2;
                    nIter2++;
                }

                if (Comp(perm1, perm2) && perm1.Count > maxLength) maxLength = perm1.Count;

                Console.WriteLine($"{string.Join(',', perm1)}  {string.Join(',', perm2)}");
            }
        }

        Console.WriteLine(maxLength);
    }

    private static bool Comp(List<int> a, List<int> b)
    {
        if (a.Count != b.Count) return false;
        for (int i = 0; i < a.Count; i++)
        {
            if (a[i] != b[i]) return false;
        }
        return true;
    }
}