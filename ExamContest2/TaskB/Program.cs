﻿using System;
using System.Collections.Generic;

class Program
{
    public static void Main(string[] args)
    {
        List<int> profit = new List<int>();
        string[] profitStrings = Console.ReadLine().Split();
        int sum = 0;
        foreach (string p in profitStrings)
        {
            profit.Add(int.Parse(p));
            sum += int.Parse(p);
        }
        Console.WriteLine(Math.Round((double)sum / profit.Count, 3));
    }
}