﻿using System;

namespace B
{
    class Program
    {
        static void Main()
        {
            long sum = 0;
            long numb;
            string input;
            bool broken = false;

            do
            {
                input = Console.ReadLine();
                if (long.TryParse(input, out numb))
                {
                    if (numb % 2 != 0) sum += numb;
                }

                else
                {
                    broken = true;
                    break;
                }
            } while (numb != 0);

            if (broken) Console.WriteLine("Incorrect input");
            else Console.WriteLine(sum);
        }
    }
}
