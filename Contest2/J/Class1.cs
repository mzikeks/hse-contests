﻿using System;
using System.Collections.Generic;

partial class Program
{
    // Проверка на валидные входные данные
    private static bool ValidateData(int day, int month, int year)
    {
        var days = new List<int>() { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        if (year % 4 == 0 && year != 1800) days[1] += 1;

        return year > 1700 && year < 1801 && month > 0 && month < 13 && day <= days[month - 1] && day > 0;
    }


    private static int GetDayOfWeek(int day, int month, int year)
    {
        var days = new List<int>() { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        if (year % 4 == 0 && year != 1800) days[1] += 1;
        int daysGone = 6 + (year - 1701) + (year - 1701) / 4;

        for (int nMonth = 0; nMonth < month - 1; nMonth++) daysGone += days[nMonth];
        return (daysGone + day - 1) % 7;

    }


    private static string GetDateOfFriday(int dateOfWeek, int day, int month, int year)
    {
        var days = new List<int>() { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        if (year % 4 == 0 && year != 1800) days[1] += 1;

        int afterDays = (5 - dateOfWeek + 7) % 7;

        for (int i = 0; i < afterDays; i++)
        {
            if (day + 1 <= days[month - 1]) day++;
            else
            {
                if (month == 12)
                {
                    month = 1;
                    day = 1;
                    year++;
                }
                else
                {
                    month++;
                    day = 1;
                }
            }
        }
        return String.Format("{0:D2}.{1:D2}.{2:D4}", day, month, year);
    }


}