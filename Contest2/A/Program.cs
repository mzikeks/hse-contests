﻿using System;

namespace A
{
    class Program
    {
        static void Main(string[] args)
        {
            uint number;
            string input = Console.ReadLine();

            if (uint.TryParse(input, out number))
            {
                uint sum = 0;
                while (number > 0)
                {
                    sum += number % 10;
                    number /= 10;
                }

                Console.WriteLine(sum);
            }
            else
            {
                Console.WriteLine("Incorrect input");
            }
        }
   }
}
