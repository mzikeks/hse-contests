﻿using System;

namespace E
{
    class Program
    {
        static void Main()
        {
            if (uint.TryParse(Console.ReadLine(), out uint x))
            {
                ulong answ = 1;
                for (int i = 1; i <= x; i++) answ *= (ulong)i;
                Console.WriteLine(answ);
            }
            else
            {
                Console.WriteLine("Incorrect input");
            }
            Console.ReadLine();
        }
    }
}
