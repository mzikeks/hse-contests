﻿using System;

partial class Program
{
    // В случае, если введённый день недели не соответствует формату входных данных
    // метод должен вернуть int.MinValue.
    // Гарантируется, что int.MinValue не может быть получен как верный ответ.

    static int MorningWorkout(String dayOfWeek, int firstNumber, int secondNumber)
    {
        if (dayOfWeek == "Monday" | dayOfWeek == "Wednesday" | dayOfWeek == "Friday")
        {
            return GetSumOfOddOrEvenDigits(firstNumber, 1);
        }

        else if (dayOfWeek == "Tuesday" | dayOfWeek == "Thursday")
        {
            return GetSumOfOddOrEvenDigits(secondNumber, 0);
        }

        else if (dayOfWeek == "Saturday")
        {
            return Maximum(firstNumber, secondNumber);
        }

        else if (dayOfWeek == "Sunday")
        {
            return Multiply(firstNumber, secondNumber);
        }

        else return int.MinValue;
    }

    static int GetSumOfOddOrEvenDigits(int value, int remainder)
    {
        //if (value < 0) value = -value;
        int answ = 0;
        while (value != 0)
        {
            if (value % 10 != 0 & remainder > 0) answ += value % 10;
            value /= 10;
        }
        return answ;
    }

    static int Multiply(int firstValue, int secondValue)
    {
        return firstValue * secondValue;
    }

    static int Maximum(int firstValue, int secondValue)
    {
        if (firstValue > secondValue) return firstValue;
        return secondValue;
    }
}