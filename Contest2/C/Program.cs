﻿using System;

namespace C
{
    class Program
    {
        static void Main()
        {
            string a = Console.ReadLine();
            string b = Console.ReadLine();
            int x, y;

            if (int.TryParse(a, out x) & int.TryParse(b, out y) & x < y)
            {
                for (int i = x; i < y; i++)
                {
                    if (i % 2 == 0) Console.WriteLine(i);
                }
            }
            else
            {
                Console.WriteLine("Incorrect input");
            }
            Console.ReadLine();
        }
    }
}
