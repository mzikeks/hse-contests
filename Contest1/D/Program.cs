﻿using System;

partial class Program
{
    static void Main(string[] args)
    {
        string input1 = Console.ReadLine();
        string input2 = Console.ReadLine();

        if (!double.TryParse(input1, out double n) || !int.TryParse(input2, out int k) || k < 0)
        {
            Console.WriteLine("Incorrect input");
        }
        else
        {
            Console.WriteLine(Pow(n, k));
        }
        Console.ReadLine();
    }
    static double Pow(double n, int k)
    {
        if (k == 0) return 1;
        else if (k % 2 == 0)
        {
            double res = Pow(n, k / 2);
            return res*res;
        }
        else return n * Pow(n, k - 1);
    }
}
