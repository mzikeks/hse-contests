﻿using System;

partial class Program
{
    static void Main(string[] args)
    {
        if (!double.TryParse(Console.ReadLine(), out double x))
        {
            Console.WriteLine("Incorrect input");
            return;
        }
        double ai = (x*x*x*x)/(4*3*2);
        int i = 0;
        double answer = 0;
        while (true)
        { 
            answer += ai;
            if (Math.Abs(ai*(-x * x * x) / ((3 * i + 5) * (3 * i + 6) * (3 * i + 7))) < double.Epsilon*2)
            {
                break;
            }
            ai *= (-x * x * x) / ((3 * i + 5) * (3 * i + 6) * (3 * i + 7));
            i++;
        }
        Console.WriteLine(answer);
        Console.ReadLine();
    }
}