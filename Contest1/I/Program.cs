﻿using System;
using System.Text;

partial class Program
{
    static void Main(string[] args)
    {
        string input = Console.ReadLine();
        Console.WriteLine(Encrypt(input));
        Console.ReadLine();
    }
    private static string Encrypt(string input)
    {
        char theMostFrequent = 'a';
        char theMostRearest = 'b';
        int max = -1;
        int min = 99999999;
        int[] counts = new int[(int)'z'];
        foreach (char symb in input) counts[symb]++;
        for(int i = 0; i < counts.Length; i++) 
        {
            if (counts[i] != 0 && counts[i] < min)
            {
                min = counts[i];
                theMostRearest = (char)i;
            }
            if (counts[i] != 0 && counts[i] > max)
            {
                max = counts[i];
                theMostFrequent = (char)i;
            }
        }
        var sb = new StringBuilder();
        foreach (char symb in input)
        {
            if (symb == theMostFrequent) sb.Append(theMostRearest);
            else if (symb == theMostRearest) sb.Append(theMostFrequent);
            else sb.Append(symb);
        }
        return sb.ToString();


    }
}