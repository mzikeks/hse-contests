﻿using System;
using System.Linq;
using System.Net.NetworkInformation;

namespace E
{
    class Program
    {
        static void Main(string[] args)
        {
            int length;
            if (!int.TryParse(Console.ReadLine(), out length) || !IsArrayLengthCorrect(length))
            {
                Console.WriteLine("Incorrect input");
            }
            int[] arr;
            if (!GenerateArray(length, out arr))
            {
                Console.WriteLine("Incorrect input");
            }
            double average = GetArrayAverage(arr);
            Console.WriteLine(GetSumOfNumbersLessThanAverage(arr, average));
            Console.ReadLine();
        }

        static bool IsArrayLengthCorrect(int length)
        {
            return length > 0;
        }

        static bool GenerateArray(int length, out int[] arr)
        {
            arr = new int[length];
            for (int i = 0; i < length; i++)
            {
                if (!int.TryParse(Console.ReadLine(), out arr[i])) return false;
            }
            return true;
        }

        public static double GetArrayAverage(int[] arr)
        {
            return (double)arr.Sum() / arr.Length;
        }

        public static int GetSumOfNumbersLessThanAverage(int[] arr, double average)
        {
            int sum = 0;
            foreach (int element in arr)
            {
                if (element < average) sum += element;
            }
            return sum;
        }
    }
    

}
