﻿using System.IO;

public static partial class Program
{
    public static void Main()
    {
        const string inputFilePath = "rick.in";
        const string outputFilePath = "morty.out";
        WriteCount(outputFilePath, GetCountCapitalLetters(inputFilePath));
    }

    private static int GetCountCapitalLetters(string inputPath)
    {
        string input = File.ReadAllText(inputPath);
        int count = 0;
        foreach (char symb in input) if (symb >= 'A' && symb <= 'Z') count++;
        return count;
    }

    private static void WriteCount(string outputPath, int count)
    {
        File.WriteAllText(outputPath, count.ToString());
    }
}