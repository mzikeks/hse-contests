﻿using System;

partial class Program
{
    public static void Main(string[] args)
    {
        int[] arr = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int k = int.Parse(Console.ReadLine());
        Console.WriteLine(Count(arr, k));
        Console.ReadLine();
    }
    static int Count(int[] arr, int k)
    {
        int count = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            for (int j = i+1; j < arr.Length; j++)
            {
                if (arr[i] + arr[j] == k) count++;
            }
        }
        return count;
    }
}