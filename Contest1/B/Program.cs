﻿using System;

namespace B
{
    class Program
    {
        static void Main(string[] args)
        {
            string input1 = Console.ReadLine();
            string input2 = Console.ReadLine();
            string input3 = Console.ReadLine();

            if (!uint.TryParse(input1, out uint a) || !uint.TryParse(input2, out uint b) || !uint.TryParse(input3, out uint c) ||
                a + b <= c || a + c <= b || b + c <= a)
            {
                Console.WriteLine("Incorrect input");
            }
            else
            {
                Console.WriteLine(a + b + c);
            }
        }
    }
}
