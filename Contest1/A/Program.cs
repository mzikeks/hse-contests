﻿using System;

namespace A
{
    class Program
    {
        static void Main()
        {
            string input1 = Console.ReadLine();
            string input2 = Console.ReadLine();
            if (!short.TryParse(input1, out short a) || !short.TryParse(input2, out short b))
            {
                Console.WriteLine("Incorrect input");
            }
            else
            {
                Console.WriteLine(a ^ b);
            }

        }
    }
}
