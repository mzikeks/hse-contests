﻿using System;
using System.IO;
using System.Text;

partial class Program
{
    public static void Main(string[] args)
    {
        CountInFile("input.txt", out int linesCount, out int wordsCount, out int charsCount);

        Console.WriteLine(linesCount);
        Console.WriteLine(wordsCount);
        Console.WriteLine(charsCount);
        Console.ReadLine();
    }

    private static readonly string[] Separators = { " ", ". ", ", ", "? ", "! ", ": ", "; " };

    private static void CountInFile(string filePath, out int linesCount, out int wordsCount, out int charsCount)
    {
        string[] lines = File.ReadAllLines(filePath, Encoding.UTF8);
        linesCount = lines.Length;
        wordsCount = 0;
        charsCount = 0;
        foreach (string line in lines)
        {
            string[] words = line.Split(Separators, StringSplitOptions.None);
            wordsCount += words.Length;
            charsCount += line.Length;
        }
    }
}