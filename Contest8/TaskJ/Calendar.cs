﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Calendar
{
    public event Action<DateTime> EveryDayNotification;

    public void Simulate(DateTime startTime, DateTime endTime)
    {
        var currentDay = startTime;
        while (currentDay <= endTime)
        {
            EveryDayNotification?.Invoke(currentDay);
            currentDay = currentDay.AddDays(1);
        }
    }
}       