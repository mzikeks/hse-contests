﻿using System;

public delegate void Calc(ref double x);

class StackCalculator
{
    public static void CreateRules(int[] args)
    {
        foreach (int fId in args)
        {
            switch (fId)
            {
                case (0):
                    Program.func += (ref double x) => x = Math.Sin(x);
                    break;
                case (1):
                    Program.func += (ref double x) => x = Math.Cos(x);
                    break;
                case (2):
                    Program.func += (ref double x) => x = Math.Tan(x);
                    break;
            }
        }
    }
}