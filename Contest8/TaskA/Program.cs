﻿using System;

delegate double Calculate(int n);

class Program
{
    public static void Main(string[] args)
    {
        int input = int.Parse(Console.ReadLine());
        Func<int, int, double> mult = (x, i) =>
        {
            double mult = 1;
            for (int j = 1; j <= 5; j++)
            {
                mult *= (i + 42) * x / (double)(j*j);
            }
            return mult;
        };

        Calculate sum = x =>
        {
            double sum = 0;
            for (int i = 1; i <= 5; i++)
            {
                sum += mult(x, i);
            }
            return sum;
        };

        Console.WriteLine("{0:F3}", sum(input));
    }
}