﻿using System;

public abstract class Mob
{
    public int HP { get; set; }
    public int Attack { get; set; }

    public Mob(int hp, int attack)
    {
        this.HP = hp;
        this.Attack = attack;
    }
    
    public void AttackMob(Mob other)
    {
        Console.WriteLine($"{this.ToString()} attacked {other.ToString()}");
    }
}