﻿using System;

public class Monster : Mob
{
    protected int Position { get; set; }

    public Monster(int hp, int attack, int position) : base(hp, attack)
    {
        Position = position;
    }

    public void AttackHero(Mob hero, int position)
    {
        if (position == Position)
        {
            Console.WriteLine($"Mario meet monster on {Position}");
            while (this.HP > 0 && hero.HP > 0)
            {
                hero.AttackMob(this);
                this.AttackMob(hero);
                this.HP -= hero.Attack;
                hero.HP -= this.Attack;
            }
        }
    }

    public override string ToString()
    {
        return $"Monster with HP = {HP} and attack = {Attack}";
    }
}