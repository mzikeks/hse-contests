﻿using System;

public delegate void AtackHeroOnPosition(Mob hero, int position);
public class Game
{
    public AtackHeroOnPosition attackHero;
    private int CastlePosition { get; set; }
    private int CountOfMonster { get; set; }
    private Hero hero;
    private Boss boss;

    public Game(int castlePosition, int countOfMonster, Hero hero, Boss boss)
    {
        this.CastlePosition = castlePosition;
        this.CountOfMonster = countOfMonster;
        this.hero = hero;
        this.boss = boss;
    }
    public void Play()
    {
        for (int coord = 0; coord < CastlePosition; coord++)
        {
            if (hero.HP <= 0)
            {
                Console.WriteLine("You Lose! Game over!");
                return;
            }
            attackHero?.Invoke(hero, coord);
        }
        while (boss.HP > 0 && hero.HP > 0)
        
        {
            hero.AttackMob(boss);
            boss.AttackMob(hero);
            boss.HP -= hero.Attack;
            hero.HP -= boss.Attack;
            if (hero.HP <= 0)
            {
                Console.WriteLine("You Lose! Game over!");
                return;
            }
        }
        
        if (hero.IsHpMoreThen75()) Console.WriteLine("You win! Princess released!");
        else if (hero.HP > 0) Console.WriteLine("Thank you, Mario! But our princess is in another castle... You lose!");
    }
}
