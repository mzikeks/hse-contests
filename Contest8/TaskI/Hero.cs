﻿using System;

public class Hero : Mob
{
    private double starthp;
    public Hero(int hp, int attack) : base(hp,attack)
    {
        starthp = hp;
    }

    public bool IsHpMoreThen75()
    {
        return HP/starthp >= 0.75;
    }

    public override string ToString()
    {
        return $"Mario with HP = {HP} and attack = {Attack}";
    }
}

