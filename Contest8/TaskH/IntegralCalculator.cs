﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class IntegralCalculator
{
    public static void InsertParameter(int param)
    {
        switch (param)
        {
            case (0):
                Program.func = GetIntegral(Math.Sin);
                break;
            case (1):
                Program.func = GetIntegral(Math.Cos);
                break;
            case (2):
                Program.func = GetIntegral(Math.Tan);
                break;
        }
    }
    private static Integral GetIntegral(Func<double, double> f)
    {
        return (double a, double b) =>
        {
            double sum = 0;
            for (double left = a; left < b; left+=Program.EPSYLON)
            {
                sum += 0.5 * Program.EPSYLON * (f(left + Program.EPSYLON) + f(left));
            }
            return sum;
        };
    }
}

