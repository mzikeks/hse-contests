﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

partial class Program
{
    public static int SecondInArray(int[] arr)
    {
        try
        {
            Array.Sort(arr);    
            return arr[^2];
        }
        catch
        {
            throw new ArgumentException("Not enough elements");
        }
    }
}

