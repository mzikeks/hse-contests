﻿using System;

internal class Hipster
{
    private int money;
    private int donate;

    public Hipster(int money, int donate)
    {
        this.money = money;
        this.donate = donate;
    }

    public int GetMoney()
    {
        donate = Math.Min(money, donate);
        money -= donate;
        return donate;
    }
}