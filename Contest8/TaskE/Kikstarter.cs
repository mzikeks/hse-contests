﻿using System;
using System.Collections.Generic;

class Kikstarter
{
    // Данный тип необходимо обязательно использовать
    public delegate int GetMoneyDelegate();

    List<GetMoneyDelegate> getMoney = new List<GetMoneyDelegate>();
    private int m;

    public Kikstarter(int m, Hipster[] hipsters)
    {
        this.m = m;
        foreach (Hipster hipster in hipsters) getMoney.Add(hipster.GetMoney);
    }

    public int Run()
    {
        if (getMoney.Count == 0) throw new ArgumentException("Not enough hipsters");
        int n = 0;
        while (m > 0)
        {
            int sum = 0;
            n++;
            foreach (var get in getMoney) sum += (int)get?.Invoke();
            m -= sum;

            if (sum == 0) throw new ArgumentException("Not enough money");
        }
        return n;
    }
}