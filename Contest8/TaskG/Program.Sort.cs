﻿using System;

partial class Program
{
    internal static int[] StrangeSort(int[] arr)
    {
        var sortedArr = new int[arr.Length];
        for (int i = 0; i < arr.Length; i++) sortedArr[i] = arr[i];
        Array.Sort(sortedArr, new Comparison<int>((int x, int y) => (x.ToString().Length > y.ToString().Length) ? 1 : -1));
        return sortedArr;
    }
}