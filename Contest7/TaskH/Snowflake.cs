﻿using System;
using System.Collections.Generic;
using System.Text;

public class Snowflake
{
    private int width;
    private int raysCount; 
    private List<char[]> output = new List<char[]>();

    public Snowflake(int widthAndHeight, int raysCount)
    {
        this.width = widthAndHeight;
        this.raysCount = raysCount;

        if (width < 1 || width % 2 == 0) throw new ArgumentException("Incorrect input");
        if (Math.Abs(Math.Log2(raysCount) - (int)Math.Log2(raysCount)) > 0.0001 || raysCount < 4) throw new ArgumentException("Incorrect input");
    }

    public override string ToString()
    {
        for (int i = 0; i < width; i++) output.Add(new string(' ', width).ToCharArray());

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (i == width / 2 || j == width / 2) output[i][j] = '*';
            }
        }

        int iterations = (int)(raysCount)/8;

        for (int i = 0; i < iterations; i++)
        {
            DrawUp(width / 2 - i * 2, width / 2);
            //Console.WriteLine(FormatArray(output));
            DrawDown(width / 2 + i * 2, width / 2);
            //Console.WriteLine(FormatArray(output));
            DrawLeft(width / 2, width / 2 - i * 2);
            //Console.WriteLine(FormatArray(output));
            DrawRight(width / 2, width / 2 + i * 2);
            //Console.WriteLine(FormatArray(output));
        }

        return FormatArray(output);
    }

    private void DrawRight(int xStart, int yStart)
    {
        for (int i = 0; i < width - yStart; i++)
        {
            output[xStart - i][yStart + i] = '*';
            output[xStart + i][yStart + i] = '*';
        }
    }

    private void DrawLeft(int xStart, int yStart)
    {
        for (int i = 0; i <= yStart; i++)
        {
            output[xStart - i][yStart - i] = '*';
            output[xStart + i][yStart - i] = '*';
        }
    }

    private void DrawDown(int xStart, int yStart)
    {
        for (int i = 0; i < width - xStart; i++)
        {
            output[xStart + i][yStart - i] = '*';
            output[xStart + i][yStart + i] = '*';
        }
    }

    private void DrawUp(int xStart, int yStart)
    {
        //Console.WriteLine($"{xStart}, {yStart}");
        for (int i = 0; i <= xStart; i++)
        {
            output[xStart - i][yStart - i] = '*';
            output[xStart - i][yStart + i] = '*';
        }
    }

    private string FormatArray(List<char[]> output)
    {
        var sb = new StringBuilder();
        foreach (char[] str in output)
        {
            sb.Append(string.Join(" ", str) + Environment.NewLine);
        }
        return sb.ToString();
    }
}