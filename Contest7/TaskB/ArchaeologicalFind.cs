using System;
using System.Collections.Generic;
#pragma warning disable

public class ArchaeologicalFind
{
    int age;
    int weight;
    string name;
    int index;

    public ArchaeologicalFind(int age, int weight, string name)
    {
        if (age <= 0) throw new ArgumentException("Incorrect age");
        if (weight <= 0) throw new ArgumentException("Incorrect weight");
        if (name == "?") throw new ArgumentException("Undefined name");
        this.age = age;
        this.name = name;
        this.weight = weight;
        this.index = TotalFindsNumber;
    }

    public static int TotalFindsNumber = 0;

    /// <summary>
    /// Добавляет находку в список.
    /// </summary>
    /// <param name="finds">Список находок.</param>
    /// <param name="archaeologicalFind">Находка.</param>
    public static void AddFind(ICollection<ArchaeologicalFind> finds, ArchaeologicalFind archaeologicalFind)
    {
        TotalFindsNumber++;
        foreach (ArchaeologicalFind find in finds)
        {
            if (find.Equals(archaeologicalFind))
            {
                return;
            }
        }

        finds.Add(archaeologicalFind);
    }


    public override bool Equals(object obj)
    {
        if (obj is ArchaeologicalFind)
        {
            var obj2 = (ArchaeologicalFind)obj;
            return this.name == obj2.name && this.age == obj2.age && this.weight == obj2.weight;
        }
        return false;

    }

    public override string ToString() => $"{this.index} {this.name} {this.age} {this.weight}";
}