﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


class RandomProxy
{
    StreamWriter log; 
    Dictionary<string, int> users = new Dictionary<string, int>();
    Random random = new Random(1579);

    public RandomProxy(StreamWriter log)
    {
        this.log = log;
    }

    public void Register(string login, int age)
    {
        if (users.ContainsKey(login)) throw new ArgumentException($"User { login }: login is already registered");
        users.Add(login, age);
        log.WriteLine($"User { login }: login registered");
    }

    public int Next(string login)
    {
        if (!users.ContainsKey(login)) throw new ArgumentException($"User { login }: login is not registered");
        int number;
        int age = users[login];
        if (age < 20) number = random.Next(0, 1000);
        else number = random.Next(0, int.MaxValue);
        log.WriteLine($"User {login}: generate number {number}");
        return number;
    }

    public int Next(string login, int maxValue)
    {
        if (!users.ContainsKey(login)) throw new ArgumentException($"User { login }: login is not registered");

        int age = users[login];
        if (age < 20 && maxValue > 1000) throw new ArgumentOutOfRangeException($"User {login}: random bounds out of range");
        int number = random.Next(0, maxValue);
        log.WriteLine($"User {login}: generate number {number}");
        return number;
    }

    public int Next(string login, int minValue, int maxValue)
    {
        if (!users.ContainsKey(login)) throw new ArgumentException($"User { login }: login is not registered");

        int age = users[login];
        if (age < 20 && maxValue-minValue > 1000) throw new ArgumentOutOfRangeException($"User {login}: random bounds out of range");
        int number = random.Next(minValue, maxValue);
        log.WriteLine($"User {login}: generate number {number}");
        return number;
    }

}
