﻿using System;
using System.Collections.Generic;

public class RegularExpression
{
    private readonly string regularExpression;
    private int regularExpressionLength = 0;
    private List<string> replacements = new List<string>();

    public RegularExpression(string expresion)
    {
        this.regularExpression = expresion;
        int i = 0;
        bool ignore = false;
        while (i < regularExpression.Length)
        {
            char symbol = regularExpression[i];
            if (ignore)
            {
                ignore = false;
            }
            else if (symbol == '\\')
            {
                ignore = true;
                i++;
                continue;
            }
            else if (symbol == '[')
            {
                int endIndex = GetIndexOfBracket(regularExpression.Substring(i, regularExpression.Length - i));
                i += endIndex;
            }
            else if (symbol == '(' || symbol == ')')
            {
                regularExpressionLength--;
            }
            regularExpressionLength++;
            i++;
        }
    }

    public string FindAndReplace(string textString, string replace)
    {
        int i = 0;
        var text = new List<char>(textString);
        var newText = new List<char>();
        while (i < text.Count)
        {
            replacements = new List<string>();
            char symbol = text[i];
            if (i + regularExpressionLength <= text.Count && CheckStringFrom(i, text))
            {
                bool isExpression = false;
                for (int j = 0; j < replace.Length; j++)
                {
                    if (isExpression == true)
                    {
                        int replacementsInd = int.Parse(replace[j].ToString()) - 1;
                        for (int k = 0; k < replacements[replacementsInd].Length; k++)
                            newText.Add(replacements[replacementsInd][k]);
                        isExpression = false;
                    }
                    else if (replace[j] == '\\') isExpression = true;
                    else newText.Add(replace[j]);
                }
                i += regularExpressionLength - 1;
            }
            else newText.Add(symbol);
            i++;
        }
        return string.Join("", newText);
    }

    public bool CheckStringFrom(int stringIndex, List<char> text)
    {
        bool ignore = false;
        int i = 0;
        bool isInExpression = false;
        List<char> innerExpression = new List<char>();

        while (i < regularExpression.Length)
        {
            char symbol = regularExpression[i];
            if (ignore)
            {
                if (text[stringIndex] != symbol) return false;
                if (isInExpression) innerExpression.Add(text[stringIndex]);
                ignore = false;
            }
            else if (symbol == '\\')
            {
                ignore = true;
                i++;
                continue;
            }
            else if (symbol == '?')
            {
                if (isInExpression) innerExpression.Add(text[stringIndex]);
            }
            else if (symbol == '[')
            {
                int endIndex = GetIndexOfBracket(regularExpression.Substring(i, regularExpression.Length - i));
                string subRegExp = regularExpression.Substring(i + 1, endIndex - 1);
                if (!CheckSquareBrackets(subRegExp, text[stringIndex])) return false;
                if (isInExpression) innerExpression.Add(text[stringIndex]);
                i += endIndex;
            }
            else if (symbol == '(')
            {
                isInExpression = true;
                i++;
                continue;
            }
            else if (symbol == ')')
            {
                isInExpression = false;
                replacements.Add(string.Join("", innerExpression));
                innerExpression = new List<char>();
                i++;
                continue;
            }
            else
            {
                if (text[stringIndex] != symbol) return false;
                if (isInExpression) innerExpression.Add(text[stringIndex]);
            }
            i++;
            stringIndex++;
        }
        return true;
    }

    private int GetIndexOfBracket(string v)
    {
        bool ignore = false;
        for (int i = 0; i < v.Length; i++)
        {
            char symbol = v[i];
            if (ignore) 
            {
                ignore = false;
                continue;
            }
            else if (symbol == '\\') ignore = true;
            else if (symbol == ']') return i;
            else ignore = false;
        }
        return -1;
    }

    private bool CheckSquareBrackets(string subRegExp, char v)
    {
        bool flag = true;
        if (subRegExp[0] == '!') flag = false;
        if (CheckQuestion(subRegExp)) return flag;
        if (v == '!' || v == '?' || v == '[' || v == ']' ||
            v == '(' || v == ')' || v == '-' || v == '\\')
        {
            string v1 = "\\" + v; 
            if (subRegExp.Contains(v1)) return flag;
        }
        else
        {
            if (subRegExp.Contains(v)) return flag;
            if (subRegExp.Contains("a-z") && v >= 'a' && v <= 'z') return flag;
            if (subRegExp.Contains("A-Z") && v >= 'A' && v <= 'Z') return flag;
            if (subRegExp.Contains("0-9") && v >= '0' && v <= '9') return flag;
        }
        
        return !flag;
    }

    private bool CheckQuestion(string s)
    {
        bool ignore = false;
        foreach (char symbol in s)
        {
            if (ignore)
            {
                ignore = false;
                continue;
            }
            else if (symbol == '\\') ignore = true;
            else if (symbol == '?') return true;
            else ignore = false;
        }
        return false;
    }
}
