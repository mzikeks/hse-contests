using System;

class Boat
{
    internal bool IsAtThePort;
    protected int cost;

    public Boat(int value, bool isAtThePort)
    {
        this.IsAtThePort = isAtThePort;
        this.cost = value;
    }

    public virtual int CountCost(int weight)
    {
        return weight * cost;
    }
}


