using System;

class Brigantine : Boat
{

    public Brigantine(int value, bool isAtThePort) : base(value, isAtThePort)
    {
    }

    new public int CountCost(int weight)
    {
        if (weight > 500) return base.CountCost(weight);
        return base.CountCost(weight) * cost;
    }
}