using System;
using System.Collections.Generic;
using System.IO;

public partial class Program
{
    /// <summary>
    /// Считывает точки в список.
    /// </summary>
    /// <returns>Список точек.</returns>
    private static List<Point> GetPoints()
    {
        var points = new List<Point>();
        using var sr = new StreamReader(InputPath);
        while (!sr.EndOfStream)
        {
            string[] data = sr.ReadLine().Split();
            points.Add(new Point(int.Parse(data[0]), int.Parse(data[1]), int.Parse(data[2])));
        }
        return points;       
    }


    /// <summary>
    /// Получает коллекцию уникальных точек.
    /// </summary>
    /// <param name="points">Список исходных точек.</param>
    /// <returns>Коллекция точек.</returns>
    private static HashSet<Point> GetUnique(List<Point> points)
    {
        var pointsSet = new HashSet<Point>();
        foreach (Point point in points) pointsSet.Add(point);
        return pointsSet;
    }
}