using System;

public class Point
{
    private int x;
    private int y;
    private int z;

    public Point(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public override bool Equals(object obj) => obj.ToString() == this.ToString();

    public override int GetHashCode() => string.GetHashCode(this.ToString());

    public override string ToString() => $"{x} {y} {z}";
}