﻿using System;


public class Group
{
    Student[] students;
    public Group(Student[] students)
    {
        if (students.Length < 5)
        {
            throw new ArgumentException("Incorrect group");
        }
        this.students = students;
    }

    public int IndexOfMinGrade()
    {
        int minGrade = 11;
        int minGradeIndex = -1;
        for (int i = 0; i < students.Length; i++)
        {
            Student student = students[i];
            if (student.grade < minGrade)
            {
                minGrade = student.grade;
                minGradeIndex = i;
            }
        }
        return minGradeIndex;
    }

    public int IndexOfMaxGrade()
    {
        int maxGrade = -1;
        int maxGradeIndex = -1;
        for (int i = 0; i < students.Length; i++)
        {
            Student student = students[i];
            if (student.grade > maxGrade)
            {
                maxGrade = student.grade;
                maxGradeIndex = i;
            }
        }
        return maxGradeIndex;
    }

    public Student this[int index]
    {
        get { return students[index]; }
    }

}