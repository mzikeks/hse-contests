﻿using System;

public class Student
{
    internal int grade;
    internal string name;

    private Student(string name, int grade)
    {
        this.name = name;
        this.grade = grade;
    }

    public static Student Parse(string line)
    {
        string[] words = line.Split();

        if (!int.TryParse(words[1], out int grade))
        {
            throw new ArgumentException("Incorrect input mark");
        }
        if (words[0][0].ToString() == words[0][0].ToString().ToLower() || words[0].Length < 3)
        {
            throw new ArgumentException("Incorrect name");
        }
        if (grade > 10 || grade < 0)
        {
            throw new ArgumentException("Incorrect mark");
        }
        return new Student(words[0], grade);
    }

    public override string ToString()
    {
        return $"{name} got a grade of {grade}.";
    }
}