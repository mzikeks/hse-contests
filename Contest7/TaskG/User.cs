using System;
using System.Collections.Generic;
using System.Text;

public class User
{
    public string name;
    private List<string> oldMessages = new List<string>();
    public bool isExist = true;

    public User(string username)
    {
        this.name = username;
    }

    public override string ToString() => $"-{name}-";


    public void SendMessage(string text)
    {
        var sb = new StringBuilder();
        sb.Append(this + Environment.NewLine);

        if (oldMessages.Count > 0) sb.Append("Received notifications:" + Environment.NewLine);

        foreach (string message in oldMessages)
        {
            sb.Append(message + Environment.NewLine);
        }

        sb.Append($"New notification: {text}");
        oldMessages.Add(text);

        Console.WriteLine(sb.ToString());
    }
}