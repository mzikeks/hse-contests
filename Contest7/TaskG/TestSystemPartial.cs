using System;
using System.Collections.Generic;

public partial class TestSystem
{
    List<User> users = new List<User>();
    public TestSystem()
    {
        Notifications += HandleNotificationEvent;
    }

    private void HandleNotificationEvent(string message)
    {
        foreach (User user in users)
        {
            if (user.isExist) user.SendMessage(message);
        }
    }

    public void Add(string username)
    {
        foreach (User user in users) 
        {
            if (user.name == username)
            {
                if (user.isExist == false)
                {
                    users.Remove(user);
                    users.Add(user);
                    user.isExist = true;
                    return;
                }
                throw new ArgumentException("User already exists");
            } 
        }

    
        users.Add(new User(username));
    }
    
    public void Remove(string username)
    {
        foreach (User user in users)
        {
            if (user.name == username)
            {
                user.isExist = false;
                return;
            }
        }
        throw new ArgumentException("User does not exist");
    }



}