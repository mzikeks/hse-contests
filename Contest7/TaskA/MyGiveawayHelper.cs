﻿using System;

internal class MyGiveawayHelper
{
    string[] prizes;
    string[] logins;
    int random = 1579;
    int nextPrize = 0;

    public MyGiveawayHelper(string[] logins, string[] prizes)
    {
        this.prizes = prizes;
        this.logins = logins;
    }

    public bool HasPrizes 
    {
         get { return nextPrize < prizes.Length; }
    }


    public (string prize, string login) GetPrizeLogin()
    {
        return (prizes[nextPrize++], logins[GetNextRandomNumber() % logins.Length]);
    }

    private int GetNextRandomNumber()
    {
        random = random * random;
        random = (random / 100 % 10) + (random / 1000 % 10) * 10 + (random / 10000 % 10) * 100 + (random / 100000 % 10) * 1000;
        return random;
    }
}   