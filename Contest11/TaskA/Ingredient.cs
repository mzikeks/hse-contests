using System;
using System.Runtime.Serialization;

[KnownType(typeof(Vegetable))]
[KnownType(typeof(Meat))]
[DataContract]
public class Ingredient: IComparable<Ingredient>
{
    [DataMember(Name = "Name")]
    public string Name { get; set; }

    [DataMember(Name = "TimeToCook")]
    protected int TimeToCook { get; set; }

    public Ingredient() { }

    public Ingredient(string name, int timeToCook)
    {
        Name = name;
        TimeToCook = timeToCook;
    }

    public override string ToString() => Name;

    public int CompareTo(Ingredient ing2)
    {
        if (this.TimeToCook == ing2.TimeToCook) return 0;
        return this.TimeToCook > ing2.TimeToCook ? -1: 1;
    }
}