using System;

public class Soup
{
    public Soup(Ingredient[] ingredients)
    {
        foreach (Ingredient ingredient in ingredients)
        {
            if (ingredient is Meat && (ingredient as Meat).IsTasty == false) WillEat = false;
            if (ingredient is Vegetable && (ingredient as Vegetable).IsAllergicTo == true) WillEat = false;
        }
    }

    public bool WillEat { get; set; } = true;
}