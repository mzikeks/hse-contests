using System;

public class MartianInt
{
    private int value;
    private static int count = 0;
    
    public MartianInt(int value)
    {
        this.value = value;
    }

    public int Value => value;

    public static explicit operator int(MartianInt v)
    {
        return v.value + (count++);
    }

    public static implicit operator MartianInt(int v)
    {
        if (v - count >= 0) return new MartianInt(v - (count++));
        count++;
        throw new ArgumentException("Value is negative");
    }
}