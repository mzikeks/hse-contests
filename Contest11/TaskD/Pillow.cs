using System;
using System.Xml.Serialization;

public class Pillow
{

    [XmlElement(ElementName = "id")]
    public long Id { get; set; }

    [XmlElement(ElementName = "isRuined")]
    public string IsRuinedStr 
    {
        get => IsRuined ? "Yes": "No"; 
        set { }
    }

    private bool IsRuined { get; set; }


    public Pillow() { }

    public Pillow(long id, bool isRuined)
    {
        Id = id;
        IsRuined = isRuined;
    }
}