using System;
using System.Xml.Serialization;

[XmlInclude(typeof(Lamp))]
[XmlInclude(typeof(Bed))]
public abstract class Furniture
{
    [XmlElement(ElementName = "id")]
    public long Id { get; set; }

    public Furniture() { }

    protected Furniture(long id)
    {
        Id = id;
    }
}