using System;
using System.Xml.Serialization;

public class Lamp : Furniture
{
    [XmlElement(ElementName = "lifeTime")]
    public double LifeTimeSeconds
    {
        get => LifeTime.TotalSeconds;
        set { }
    }

    private TimeSpan LifeTime { get; set; }

    public Lamp() { }

    public Lamp(long id, TimeSpan lifeTime) : base(id)
    {
        LifeTime = lifeTime;
    }
}