using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class Student
{
    public string Name { get; private set; }
    public string LastName { get; private set; }
    public int GroupNumber { get; private set; }
    public List<int> Grades { get; private set; }

    public Student(string name, string lastName, int groupNumber, List<int> grades)
    {
        Name = name;
        LastName = lastName;
        GroupNumber = groupNumber;
        Grades = grades;
    }

    public static Student Create(string studentInfo)
    {
        string[] input = studentInfo.Split();
        var inputGrades = input.Skip(3).ToArray();
        return new Student(input[0], input[1], int.Parse(input[2]), Array.ConvertAll(inputGrades, int.Parse).ToList());
    }
}