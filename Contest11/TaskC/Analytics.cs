using System;
using System.Collections.Generic;

public static class Analytics
{
    public static double FindGpa(List<Student> students)
    {
        double sum = 0;
        foreach (var student in students)
        {
            foreach (var mark in student.Grades) sum += mark;
        }
        return sum / (students.Count * students[0].Grades.Count);
    }


    public static void WriteStudentsWithGpaNoLessThanAverage(List<Student> students, string path, double gpa)
    {
        Console.WriteLine(gpa.ToString("f2"));
        foreach (var student in students)
        {
            double sum = 0;
            foreach (var mark in student.Grades) sum += mark;
            if (sum / student.Grades.Count >= gpa) Console.WriteLine(student);
        }
    }
}