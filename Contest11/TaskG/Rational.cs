using System;

public class Rational
{
    int numerator;
    int denomenator;
    
    public static Rational Parse(string input)
    {
        if (input.Split("/").Length == 1) return new Rational(int.Parse(input), 1);
        return new Rational 
        (
            int.Parse(input.Split("/")[0]),
            int.Parse(input.Split("/")[1])
        );
    }

    public Rational(int numerator, int denomenator)
    {
        int gcdThis = GCD(Math.Abs(numerator), Math.Abs(denomenator));
        numerator /= gcdThis;
        denomenator /= gcdThis;
        if (denomenator < 0)
        {
            denomenator = -denomenator;
            numerator = -numerator;
        }

        this.numerator = numerator;
        this.denomenator = denomenator;    
        
    }

    private static int GCD(int one, int two)
    {
        while (one != 0 && two != 0)
        {
            if (one > two) one %= two;
            else two %= one;
        }
        return Math.Max(1, one + two);
    }

    public override string ToString()
    {
        if (denomenator == 1)
            return numerator.ToString();
        return numerator + "/" + denomenator;
    }

    public static Rational operator +(Rational one, Rational two)
    {
        return new Rational 
        ( 
            one.numerator * two.denomenator + two.numerator * one.denomenator,
            one.denomenator * two.denomenator
        );
    }

    public static Rational operator -(Rational one, Rational two)
    {
        return new Rational
        (
            one.numerator * two.denomenator - two.numerator * one.denomenator,
            one.denomenator * two.denomenator
        );
    }

    public static Rational operator *(Rational one, Rational two)
    {
        return new Rational
        (
            one.numerator * two.numerator,
            one.denomenator * two.denomenator
        );
    }

    public static Rational operator /(Rational one, Rational two)
    {
        if (one.numerator == 0 && two.numerator == 0) return new Rational(1, 1);
        return new Rational
        (
            one.numerator * two.denomenator,
            one.denomenator * two.numerator
        );
    }
}