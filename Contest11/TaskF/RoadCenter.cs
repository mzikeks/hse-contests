using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

public class RoadCenter : ITrackingCenter
{
    private List<Camera> cameras = new List<Camera>();
    public void AddCamera(int id, int maxSpeed)
    {
        cameras.Add(new Camera(id, maxSpeed));
    }

    public void CheckCarSpeed(int forCameraId, int carNumber, int carSpeed)
    {
        foreach (var camera in cameras)
        {
            if (camera.Id == forCameraId) camera.AddPenalty(carNumber, carSpeed);
        }
    }

    public void GetData(string inFilePath)
    {
        string json = JsonSerializer.Serialize<List<Camera>>(cameras);
        Console.WriteLine("{"+ $"\"cameras\":{json}" + "}");

    }
}