using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

public class Camera
{
    private int id;

    [JsonPropertyName("id")]
    public int Id => id;

    private int maxSpeed;

    private List<Penalty> penalties = new List<Penalty>();

    [JsonPropertyName("penalties")]
    public List<Penalty> Penalties => penalties;

    public Camera(int id, int maxSpeed)
    {
        this.id = id;
        this.maxSpeed = maxSpeed;
    }

    public void AddPenalty(int carNumber, int speed)
    {
        if (speed > maxSpeed)
        {
            penalties.Add(new Penalty(carNumber, 100 * (speed - maxSpeed)));
        }
    }
}