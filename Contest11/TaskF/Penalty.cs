using System;
using System.Text.Json.Serialization;

public class Penalty
{
    private int carNumber;
    [JsonPropertyName("car_number")]
    public int CarNumber => carNumber;
    
    private int cost;
    [JsonPropertyName("cost")]
    public int Cost => cost;

    public Penalty(int carNumber, int cost)
    {
        this.carNumber = carNumber;
        this.cost = cost;
    }
}