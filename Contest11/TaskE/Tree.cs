using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

[XmlInclude(typeof(Oak))]
[XmlInclude(typeof(Ash))]
public class Tree: IComparable<Tree>
{
    public int height;
    public int age;

    public Tree()
    {
    }

    public Tree(int height, int age)
    {
        this.height = height;
        this.age = age;
    }

    public int CompareTo(Tree other)
    {
        if (this.height == other.height) return 0;
        return this.height > other.height ? 1 : -1; 
    }

    public override string ToString()
    {
        return $"height:{height} age:{age}";
    }

}