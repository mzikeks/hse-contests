﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

public class Program
{
    public static void Main(string[] args)
    {
        int k = int.Parse(Console.ReadLine());
        int n = int.Parse(Console.ReadLine());

        var points = new List<Point>();
        for (int i = 0; i < n; i++)
        {
            var input = Console.ReadLine().Split();
            points.Add(new Point { x = int.Parse(input[0]), y = int.Parse(input[1]) });
        }
        points.Sort();
        var kPoints = points.Take(k).ToList();
        kPoints.Sort(comp);
        for (int i = 0; i < k; i++)
        {
            Console.WriteLine(kPoints[i]);
        }
    }

    private static int comp(Point a, Point b)
    {
        if (a.x == b.x)
        {
            if (a.y == b.y) return 0;
            else return a.y > b.y ? 1 : -1; 
        }
        return a.x > b.x ? 1 : -1;
    }
}

public class Point: IComparable<Point>
{
    public int x;
    public int y;

    public int CompareTo(Point other)
    {
        if (x * x + y * y == other.x * other.x + other.y * other.y) return 0;
        return x * x + y * y > other.x * other.x + other.y * other.y ? 1 : -1;
    }

    public override string ToString()
    {
        return $"({x}, {y})";
    }
}