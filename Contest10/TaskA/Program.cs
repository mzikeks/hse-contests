﻿using System.Collections.Generic;
using System.IO;

class Program
{
    public static void Main(string[] args)
    {
        using (StreamReader sr = new StreamReader("input.txt"))
        {
            using (BinaryWriter sw = new BinaryWriter(File.Open("output.bin", FileMode.OpenOrCreate)))
            {
                string line;
                while ((line =  sr.ReadLine()) != null)
                {
                    sw.Write(ushort.Parse(line));
                }
                
            }
        }
    }
}