using System;
using System.IO;
using System.Linq;

public class Summator
{
    int sum = 0;

    public Summator(string path)
    {
        using (StreamReader sr = new StreamReader(path, System.Text.Encoding.UTF8))
        {
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                sum += line.Split("_").Select(int.Parse).ToList().Sum();
            }
        }
    }

    public int Sum
    {
        get
        {
            return sum;
        }
    }
}