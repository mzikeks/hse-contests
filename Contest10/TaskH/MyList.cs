using System;
using System.Linq;
using System.Text;

public class MyList<T>
{
    T[] list;
    int count = 0;

    public MyList()
    {
        list = new T[0];
    }

    public MyList(int capacity)
    {
        list = new T[capacity];
    }

    public int Count => count;

    public int Capacity => list.Length;


    public void Add(T element)
    {
        if (Capacity == Count)
        {
            if (Capacity == 0) list = new T[4];
            else
            {
                T[] newList = new T[Capacity * 2];
                for (int i = 0; i < Count; i++) newList[i] = list[i];
                list = newList;
            }
        }
        list[Count] = element;
        count++;
    }

    public T this[int x]
    {
        get
        {
            if (x >= 0 && x < Count) return list[x];
            throw new IndexOutOfRangeException();
        }
    }

    public void Clear()
    {
        for (int i = 0; i < Count; i++) list[i] = default(T);
        count = 0;
    }

    public void RemoveLast()
    {
        if (Count == 0) throw new IndexOutOfRangeException();
        list[Count - 1] = default(T);
        count--;
    }

    public void RemoveAt(int index)
    {
        if (index < 0 || index >= Count) throw new IndexOutOfRangeException();
        count--;
        for (int i = index; i < count; i++) list[i] = list[i + 1];
        list[Count] = default(T);
    }

    public T Max()
    {
        if (!typeof(T).GetInterfaces().Contains(typeof(IComparable))) throw new NotSupportedException("This operation is not supported for this type");
        if (count == 0) throw new IndexOutOfRangeException();

        T maxElement = list[0];
        for (int i = 0; i < Count; i++)
        {
            var element = list[i];
            var CompElement = element as IComparable;
            if (CompElement.CompareTo(maxElement as IComparable) == -1) maxElement = element;
        }
        return maxElement;

    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < Count; i++) sb.Append(list[i].ToString() + " ");
        return sb.ToString();
    }
}