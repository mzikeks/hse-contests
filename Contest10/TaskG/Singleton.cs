﻿using System;

class Singleton<T> 
    where T: new()
{
    private static T instance;

    public static T Instance 
    {
        get
        {
            if (instance == null) instance = new T();
            return instance;
        }
        set
        {
            if (instance == null)
            {
                instance = value;
            }
            else
            {
                throw new NotSupportedException("This operation is not supported");
            }
        }
    }
}
