using System;
using System.Collections.Generic;
using System.IO;

public class Server
{
    public static void ProcessAuthorization(string requestsPath, string requestsResultsPath)
    {
        List<User> users = new List<User>();
        foreach (string username in UserDb.Users.Keys)
        {
            users.Add(new User { isAuthorized = false, username = username, password = UserDb.Users[username] });
        }
        using StreamReader sr = new StreamReader(requestsPath, System.Text.Encoding.UTF8);
        using StreamWriter sw = new StreamWriter(requestsResultsPath);

        string line;
        while ((line = sr.ReadLine()) != null)
        {
            var splitedLine = line.Split();
            if (splitedLine.Length < 2) continue;
            string action = splitedLine[0];
            User user;
            try
            {
                user = User.GetUserByUsername(users, splitedLine[1]);
            }
            catch
            {
                sw.WriteLine($"{splitedLine[1]}> no user with such login");
                continue;
            }

           
            if (action == "SO" && user.isBlocked == false)
            {
                user.isAuthorized = false;
                sw.WriteLine($"{splitedLine[1]}> sign out successful");
            }
            else if (user.isBlocked == false)
            {
                string password = splitedLine[2];
                DateTime time = DateTime.ParseExact(splitedLine[^2] + " " + splitedLine[^1],
                    "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.GetCultureInfo("ru"));

                bool isPasswordCorrect = user.password == password;
                if (user.isAuthorized && (time - user.lastTry).TotalDays < 1)
                {
                    user.isBlocked = true;
                }
                else if (!isPasswordCorrect)
                {
                    if ((time - user.lastUnsucccessfulTry).TotalHours < 1)
                    {
                        user.isBlocked = true;
                    }
                    else
                    {
                        sw.WriteLine($"{user.username}> incorrect password");
                        user.lastUnsucccessfulTry = time;
                    }
                }
                else
                {
                    user.isAuthorized = true;
                    user.lastTry = time;
                    sw.WriteLine($"{user.username}> sign in successful");
                }
            }
            if (user.isBlocked) sw.WriteLine($"{user.username}> account blocked due suspicious login attempt");
        }
    }
}

public class User
{
    public string username;
    public string password;
    public bool isAuthorized = false;
    public DateTime lastTry = DateTime.MinValue;
    public DateTime lastUnsucccessfulTry = DateTime.MinValue;
    public bool isBlocked = false;

    public static User GetUserByUsername(List<User> users, string username)
    {
        foreach (User user in users)
        {
            if (user.username == username)
            {
                return user;
            }
        }
        throw new NotImplementedException();
    }
}