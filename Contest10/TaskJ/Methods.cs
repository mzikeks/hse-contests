using System;
using System.Collections.Generic;
using System.Xml;

public class Methods
{
    static XmlDocument doc;
    static Dictionary<string, XmlElement> createdPersons = new Dictionary<string, XmlElement>();

    public static XmlDocument GetDocument(string companyNameString, List<string> persons)
    {
        doc = new XmlDocument();
        var company = doc.CreateElement("company");
        var companyName = doc.CreateTextNode(companyNameString);
        var companyNameAttr = doc.CreateAttribute("name");
        companyNameAttr.AppendChild(companyName);

        company.Attributes.Append(companyNameAttr);
        doc.AppendChild(company);
        

        company.Attributes.Append(companyNameAttr);
        doc.AppendChild(company);

        AddPerson(company, persons[0]);
        
        for (int i = 1; i < persons.Count; i++)
        {
            AddPerson(createdPersons[persons[i].Split()[1]], persons[i]);
        }

        return doc;
    }

    public static void AddPerson(XmlElement parent, string personString)
    {
        var persons1List = personString.Split('\t');

        var person = doc.CreateElement("person");

        var personId = doc.CreateTextNode(persons1List[0]);
        var personIdAttr = doc.CreateAttribute("id");

        var personName = doc.CreateTextNode(persons1List[3]);
        var personNameAttr = doc.CreateAttribute("name");

        var personPosition = doc.CreateTextNode(persons1List[2]);
        var personPositionAttr = doc.CreateAttribute("position");

        personPositionAttr.AppendChild(personPosition);
        personNameAttr.AppendChild(personName);
        personIdAttr.AppendChild(personId);

        person.Attributes.Append(personIdAttr);
        person.Attributes.Append(personNameAttr);
        person.Attributes.Append(personPositionAttr);

        createdPersons[personId.InnerText] = person;

        parent.AppendChild(person);

    }
}