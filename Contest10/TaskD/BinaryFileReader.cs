using System;
using System.IO;

public class BinaryFileReader
{
    private string path;
    private int sum16 = 0;
    private int sum32 = 0;

    public BinaryFileReader(string path)
    {
        this.path = path;

        using (BinaryReader br = new BinaryReader(File.Open(path, FileMode.Open, FileAccess.Read)))
        {
            while (br.BaseStream.Position != br.BaseStream.Length)
            {
                sum16 += br.ReadInt16();
            }
        }
        using (BinaryReader br = new BinaryReader(File.Open(path, FileMode.Open, FileAccess.Read)))
        {
            while (br.BaseStream.Position != br.BaseStream.Length)
            {
                sum32 += br.ReadInt32();
            }
        }
    }

    public int GetDifference()
    {
        return Math.Abs(sum16-sum32);
    }
}