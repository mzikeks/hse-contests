using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

public class JsonReader
{
    List<House> houses;
    double maxTemp;

    public JsonReader(string path)
    {
        using (StreamReader r = new StreamReader(path))
        {
            string json = r.ReadLine();
            maxTemp = double.Parse(r.ReadLine());
            houses = JsonSerializer.Deserialize<List<House>>(json);
        }
    }
    
    public string TheSickestStreet
    {
        get
        {
            var streets = new Dictionary<string, int>();
            foreach (var house in houses)
            {
                if (@streets.ContainsKey(house.Street)) streets[house.Street] = 0;
                foreach (var student in house.Students)
                {
                    if (student.Temperature > maxTemp) streets[house.Street]++;
                }
            }
            int maxStudents = 0;
            string streetName = null;
            foreach (var street in streets.Keys)
            {
                if (streets[street] > maxStudents)
                {
                    maxStudents = streets[street];
                    streetName = street;
                }
            }

            return streetName;
        }
    }
}