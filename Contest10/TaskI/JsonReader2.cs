﻿using System;
using System.Collections.Generic;
using System.IO;

public class JsonReader2
{
    double maxTemp;
    Dictionary<string, int> streets = new Dictionary<string, int>();

    public JsonReader2(string path)
    {
        using (StreamReader r = new StreamReader(path))
        {
            string json = r.ReadLine();
            maxTemp = double.Parse(r.ReadLine());

            var houses = json.Split("street", StringSplitOptions.RemoveEmptyEntries);
            foreach (var house in houses)
            {
                var houseElement = house.Split();
                if (houseElement.Length < 2) continue;
                string street = houseElement[1].Trim(',').Trim('"');
                if (!streets.ContainsKey(street)) streets[street] = 0;
                double temperature = 0;
                for (int i = 0; i < houseElement.Length; i++)
                {
                    var element = houseElement[i].Trim(':').Trim('"');
                    if (element == "temperature") temperature = double.Parse(houseElement[i + 1].Trim(','));
                    if (element == "isMale")
                    {
                        if (houseElement[i + 1].Replace(",", "").Replace("]", "").Replace("}", "") == "true")
                        {
                            if (temperature > maxTemp) streets[street]++;
                        }
                    }
                }
            }
        }
    }

    public string TheSickestStreet
    {
        get
        {
            int maxStudents = 0;
            string streetName = null;
            foreach (var street in streets.Keys)
            {
                if (streets[street] > maxStudents)
                {
                    maxStudents = streets[street];
                    streetName = street;
                }
            }

            return streetName;
        }
    }
}