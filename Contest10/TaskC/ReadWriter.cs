using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class ReadWriter
{
    public static Tuple<char, char> GetMostAndLeastCommonLetters(string path)
    {
        Dictionary<char, int> frequences = new Dictionary<char, int>();
        List<char> order = new List<char>();

        using (StreamReader sr = new StreamReader(path, System.Text.Encoding.UTF8))
        {
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                foreach (char b in line)
                {
                    char letter = b.ToString().ToLower()[0];
                    if (!char.IsLetter(letter)) continue;
                    if (!frequences.ContainsKey(letter)) 
                    {
                        frequences[letter] = 0;
                        order.Add(letter);
                    }
                    frequences[letter]++;
                }
            }
        }
        int maxFrequence = 0;
        char maxFrequenceLetter = 'f';
        foreach (var key in frequences.Keys) 
        { 
            if (frequences[key] > maxFrequence)
            {
                maxFrequence = frequences[key];
                maxFrequenceLetter = key;
            }
            else if (frequences[key] == maxFrequence && order.IndexOf(key) < order.IndexOf(maxFrequenceLetter))
            {
                maxFrequenceLetter = key;
            }
        }

        int minFrequence = int.MaxValue;
        char minFrequenceLetter = 'f';
        foreach (var key in frequences.Keys)
        {
            if (frequences[key] < minFrequence)
            {
                minFrequence = frequences[key];
                minFrequenceLetter = key;
            }
            else if (frequences[key] == minFrequence && order.IndexOf(key) < order.IndexOf(minFrequenceLetter))
            {
                minFrequenceLetter = key;
            }
        }
        return new Tuple<char, char>(minFrequenceLetter, maxFrequenceLetter);
    }

    public static void ReplaceMostRareLetter(Tuple<char, char> leastAndMostCommon, string inputPath, string outputPath)
    {
        using (StreamReader sr = new StreamReader(inputPath, System.Text.Encoding.UTF8))
        {
            using (StreamWriter sw = new StreamWriter(File.Open(outputPath, FileMode.OpenOrCreate)))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var sb = new StringBuilder();
                    foreach (var symb in line)
                    {
                        if (symb == leastAndMostCommon.Item1) sb.Append(leastAndMostCommon.Item2);
                        else if (symb.ToString().ToLower() == leastAndMostCommon.Item1.ToString().ToLower()) sb.Append(leastAndMostCommon.Item2.ToString().ToUpper()[0]);
                        else sb.Append(symb);
                    }
                    sw.WriteLine(sb.ToString());
                }

            }
        }
    }
}