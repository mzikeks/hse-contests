﻿using System;
using System.IO;
using System.Text;

internal class Matrix
{
    int[,] matrix;

    public Matrix(string filename)
    {
        string[] input = File.ReadAllLines(filename);
        matrix = new int[input.Length, input[0].Split(';').Length];
        for (int i = 0; i < input.Length; i++)
        {
            string[] line = input[i].Split(';');
            for (int j = 0; j < line.Length; j++)
            {
                matrix[i, j] = int.Parse(line[j]);
            }
        }
    }

    public int SumOffEvenElements
    {
        get
        {
            int sum = 0;
            for (int i = 0; i <= matrix.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= matrix.GetUpperBound(1); j++)
                {
                    if (matrix[i, j] % 2 == 0) sum += matrix[i, j];
                }
            }
            return sum;
        }
    }


    public override string ToString()
    {
        var sb = new StringBuilder();
        for (int i = 0; i <= matrix.GetUpperBound(0); i++)
        {
            for (int j = 0; j <= matrix.GetUpperBound(1); j++)
            {
                sb.Append(matrix[i, j] + "\t");
            }
            sb.Append(Environment.NewLine);
        }
        return sb.ToString();
    }
}