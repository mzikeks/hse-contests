using System;
using System.Collections.Generic;
using System.IO;

public partial class Program
{
    public static bool ParseCommandsCount(string input, out int count)
    {
        if (!int.TryParse(input, out count) || count < 1) return false;
        return true;
    }

    public class Logger
    {
        private static List<string> logs = new List<string>();
        
        public static void HandleCommand(string command)
        {
            if (command.Split().Length > 1)
            {
                logs.Add(command.Substring(command.IndexOf("<") + 1, command.LastIndexOf(">") - command.IndexOf("<") - 1));
            }

            else if (command == "WriteAllLogs")
            {
                if (logs.Count > 0) File.AppendAllLines("logs.log", logs);
                else WriteError();
                logs = new List<string>();
            }

            else if (command == "DeleteLastLog")
            {
                if (logs.Count > 0) logs.RemoveAt(logs.Count - 1);
                else WriteError();
            }
        }
        public static void WriteError()
        {
            File.AppendAllText("logs.log", "No active logs" + Environment.NewLine);
        }
    }
}