﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

partial class Program
{
    static bool TryParseVectorFromFile(string filename, out int[] vector)
    {
        string[] input = File.ReadAllText(filename).Split();
        vector = new int[input.Length];
        bool isInputCorrect = true;

        for (int i = 0; i < input.Length; i++)
        {
            if (!int.TryParse(input[i], out vector[i])) isInputCorrect = false;
        }
        return isInputCorrect;
    }

    static int[,] MakeMatrixFromVector(int[] vector)
    {
        var matrix = new int[vector.Length, vector.Length];
        for (int i = 0; i < vector.Length; i++)
        {
            for (int j = 0; j < vector.Length; j++) matrix[i, j] = vector[i] * vector[j];
        }
        return matrix;
    }

    static void WriteMatrixToFile(int[,] matrix, string filename)
    {
        var output = new StringBuilder();
        for (int i = 0; i <= matrix.GetUpperBound(0); i++)
        {
            for (int j = 0; j <= matrix.GetUpperBound(0); j++) output.Append(matrix[i, j] + " ");
            output.Append(Environment.NewLine);
        }
        File.WriteAllText(filename, output.ToString());
    }
}
