﻿using System;

public class Triangle
{
    private readonly Point a;
    private readonly Point b;
    private readonly Point c;
    private readonly double eps = 0.000005;

    private double AB => GetLengthOfSide(a, b);
    private double AC => GetLengthOfSide(a, c);
    private double BC => GetLengthOfSide(b, c);

    public Triangle(Point a, Point b, Point c)
    {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double GetPerimeter()
    {
        return AB + AC + BC;
    }

    public double GetSquare()
    {
        double p = GetPerimeter()/2;
        return Math.Sqrt(p*(p-AC)*(p-AB)*(p-BC));
    }

    public bool GetAngleBetweenEqualsSides(out double angle)
    {
        angle = -1;
        if (AB == AC)
        {
            angle = Math.Acos(-(BC * BC - AC * AC - AB * AB)/(AC * AB * 2));
            return true;
        }
        else if (AC == BC)
        {
            angle = Math.Acos(-(AB * AB - AC * AC - BC * BC) / (AC * BC * 2));
            return true;
        }
        else if (BC == AB)
        {
            angle = Math.Acos(-(AC * AC - BC * BC - AB * AB) / (BC * AB * 2));
            return true;
        }
        return false;
    }

    public bool GetHypotenuse(out double hypotenuse)
    {
        hypotenuse = -1;
        if (Math.Abs(Math.Sqrt(AB * AB + AC * AC) - BC) < eps)
        {
            hypotenuse = BC;
            return true;
        }
        if (Math.Abs(Math.Sqrt(AB * AB + BC * BC) - AC) < eps) 
        {
            hypotenuse = AC;
            return true;
        }
        if (Math.Abs(Math.Sqrt(BC * BC + AC * AC) - AB) < eps)
        {
            hypotenuse = AB;
            return true;
        }
        return false;
    }

    
    private static double GetLengthOfSide(Point first, Point second)
    {
        return Math.Sqrt(Math.Pow(first.GetX() - second.GetX(), 2) 
            + Math.Pow(first.GetY() - second.GetY(), 2));
    }
}