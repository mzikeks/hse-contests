using System;

public partial class Program
{
    static bool ParseArrayFromLine(string line, out int[] arr)
    {
        bool isArrayCorrect = true;
        string[] input = line.Split();

        arr = new int[input.Length];
        for (int i = 0; i < input.Length; i++)
        {
            if (!int.TryParse(input[i], out arr[i])) isArrayCorrect = false;
        }
        return isArrayCorrect;
    }

    private static void Merge(int[] arr, int left, int right, int mid)
    {
        int firstArrayInd = left;
        int secondArrayInd = mid;
        int[] outputArray = new int[arr.Length];

        for (int i = 0; i < arr.Length; i++) outputArray[i] = arr[i];
    
        for (int i = left; i < right; i++)
        {
            if (firstArrayInd >= mid)
            {
                outputArray[i] = arr[secondArrayInd];
                secondArrayInd++;
            }
            else if (secondArrayInd >= right)
            {
                outputArray[i] = arr[firstArrayInd];
                firstArrayInd++;
            }
            else if (arr[firstArrayInd] < arr[secondArrayInd])
            {
                outputArray[i] = arr[firstArrayInd];
                firstArrayInd++;
            }
            else
            {
                outputArray[i] = arr[secondArrayInd];
                secondArrayInd++;
            }
        }
        for (int i = 0; i < arr.Length; i++) arr[i] = outputArray[i];
    }
}