﻿using System;
using System.Collections.Generic;
using System.Text;

partial class Program
{
    static int[][] GetBellTriangle(uint rowCount)
    {
        var array = new int[rowCount][];
        array[0] = new int[] {1};
        for (int i = 1; i < rowCount; i++)
        {
            array[i] = new int[i + 1];
            array[i][0] = array[i - 1][i - 1];
            for (int j = 0; j < i; j++)
            {
                array[i][j + 1] = array[i][j] + array[i - 1][j];
            }
        }
        return array;

    }

    private static void PrintJaggedArray(int[][] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            Console.WriteLine(string.Join(" ", array[i]));
        }
    }
}

