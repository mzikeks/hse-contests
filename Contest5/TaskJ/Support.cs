using System;
using System.Collections.Generic;

public class Support
{
    private readonly List<Task> tasks = new List<Task>();
    private int id = 1;
    public IEnumerable<Task> Tasks => tasks;

    public int OpenTask(string text)
    {
        tasks.Add(new Task(id, text));
        return id++;
    }

    public void CloseTask(int id, string answer)
    {
        Task task = tasks[id - 1];
        task.Answer = answer;
        task.IsResolved = true;
    }

    public List<Task> GetAllUnresolvedTasks()
    {
        var unresolvedTasks = new List<Task>();
        foreach (Task task in tasks) if (!task.IsResolved) unresolvedTasks.Add(task);
        return unresolvedTasks;
    }

    public void CloseAllUnresolvedTasksWithDefaultAnswer(string answer)
    {
        foreach (Task task in tasks)
        {
            if (!task.IsResolved)
            {
                task.IsResolved = true;
                task.Answer = answer;
            }
        }
    }

    public string GetTaskInfo(int id)
    {
        return tasks[id - 1].ToString();
    }
}