﻿using System;
using System.Runtime.InteropServices;
using System.Text;

class Polynom
{

    public static bool TryParsePolynom(string line, out int[] arr)
    {
        string[] input = line.Split();
        arr = new int[input.Length];

        bool isCorrectInput = true;
        for (int i = 0; i < input.Length; i++)
        {
            if (!int.TryParse(input[i], out arr[i])) isCorrectInput = false;
        }
        return isCorrectInput;
    }

    public static int[] Sum(int[] a, int[] b)
    {
        if (a.Length < b.Length)
        {
            int[] newA = new int[b.Length];
            for (int i = 0; i < a.Length; i++) newA[i] = a[i];
            a = newA;
        }
        if (b.Length < a.Length)
        {
            int[] newB = new int[a.Length];
            for (int i = 0; i < b.Length; i++) newB[i] = b[i];
            b = newB;
        }

        var res = new int[a.Length];
        for (int i = 0; i < a.Length; i++) res[i] = a[i] + b[i];
        return res;
    }

    public static int[] Dif(int[] a, int[] b)
    {
        if (a.Length < b.Length)
        {
            int[] newA = new int[b.Length];
            for (int i = 0; i < a.Length; i++) newA[i] = a[i];
            a = newA;
        }
        if (b.Length < a.Length)
        {
            int[] newB = new int[a.Length];
            for (int i = 0; i < b.Length; i++) newB[i] = b[i];
            b = newB;
        }
        var res = new int[a.Length];
        for (int i = 0; i < a.Length; i++) res[i] = a[i] - b[i];
        return res;
    }

    public static int[] MultiplyByNumber(int[] a, int n)
    {
        var res = new int[a.Length];
        for (int i = 0; i < a.Length; i++) res[i] = n * a[i];
        return res;
    }

    public static int[] MultiplyByPolynom(int[] a, int[] b)
    {
        if (a.Length < b.Length)
        {
            int[] newA = new int[b.Length];
            for (int i = 0; i < a.Length; i++) newA[i] = a[i];
            a = newA;
        }
        if (b.Length < a.Length)
        {
            int[] newB = new int[a.Length];
            for (int i = 0; i < b.Length; i++) newB[i] = b[i];
            b = newB;
        }

        var res = new int[a.Length*2];
        for (int i = 0; i < a.Length; i++)
        {
            for (int j = 0; j < b.Length; j++)
            {
                res[i + j] += a[i] * b[j];
            }
        }
        return res;
    }

    public static string PolynomToString(int[] polynom)
    {
        var output = new StringBuilder();
        for (int i = polynom.Length - 1; i > 0; i--)
        {
            string coef = "";
            if (polynom[i] == 1) coef = "";
            else if (polynom[i] == -1) coef = "-";
            else if (polynom[i] != 0) coef = polynom[i].ToString();
            else continue;
            string degree = i != 1 ? i.ToString() : "";
            output.Append($"{coef}x{degree} + ");
        }
        if (polynom[0] == 0 && output.Length > 3) output.Remove(output.Length - 3, 3);
        else output.Append(polynom[0]);

        if (output.Length == 0) output.Append(0);

        return output.ToString();
    }
}
