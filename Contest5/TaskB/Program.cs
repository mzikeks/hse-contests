﻿using System;
using System.Collections.Generic;

internal static class Program
{

    private static void Main(string[] args)
    {
        string[] input = Console.ReadLine().Split(",");

        int[] array = new int[input.Length];
        for (int i = 0; i < input.Length; i++) array[i] = int.Parse(input[i]);

        int[][] output = new int[input.Length][];
        for (int i = 0; i < input.Length; i++)
        {
            output[i] = new int[input.Length];
            for (int j = i; j < i + input.Length; j++) output[i][j - i] = array[j % input.Length];
        }
        for (int i = 0; i < input.Length; i++) Console.WriteLine(string.Join("", output[i]));
        Console.ReadLine();
    }
}